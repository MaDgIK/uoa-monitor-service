function addStandaloneWithDefaultValue() {
    /* Set standalone by default true */
    db.stakeholder.updateMany({standalone: {$exists: false}}, {$set: {standalone: true}});
}

addStandaloneWithDefaultValue();
