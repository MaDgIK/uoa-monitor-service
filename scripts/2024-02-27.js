function deleteIndexParameters() {
    var indicators = db.indicator.find();
    indicators.forEach(indicator => {
        indicator.indicatorPaths.forEach(path => {
            if(path.parameters) {
                delete path.parameters['index_id'];
                delete path.parameters['index_name'];
                delete path.parameters['index_shortName'];
            }
            db.indicator.save(indicator);
        });
    })
}

function addCopyWithDefaultValue() {
    /* Set default profiles with copy: false */
    db.stakeholder.updateMany({defaultId: null, copy: {$exists: false}}, {$set: {copy: false}});
    /* Set not default profiles with copy: true */
    db.stakeholder.updateMany({defaultId: {$ne: null}, copy: {$exists: false}}, {$set: {copy: true}});
}

deleteIndexParameters();
addCopyWithDefaultValue();
