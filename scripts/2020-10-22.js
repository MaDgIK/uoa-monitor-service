function upperCaseEnumValues() {
    stakeholders = db.stakeholder.find().map(function (stakeholders) {
        return stakeholders;
    });
    for(var i=0; i<stakeholders.length; i++) {
        stakeholder = stakeholders[i];

        stakeholderType = stakeholder.type;
        stakeholder.type = stakeholderType.toUpperCase();
        db.stakeholder.save(stakeholder);
    }
    print("Uppercase enum values for Stakeholder types");

    sections = db.section.find().map(function (sections) {
        return sections;
    });
    for(var i=0; i<sections.length; i++) {
        section = sections[i];

        sectionType = section.type;
        section.type = sectionType.toUpperCase();
        db.section.save(section);
    }
    print("Uppercase enum values for Section types");

    indicators = db.indicator.find().map(function (indicators) {
        return indicators;
    });
    for(var i=0; i<indicators.length; i++) {
        indicator = indicators[i];

        indicatorType = indicator.type;
        indicator.type = indicatorType.toUpperCase();
        indicatorWidth = indicator.width;
        indicator.width = indicatorWidth.toUpperCase();

        indicatorPaths = indicator.indicatorPaths;
        for(var j=0; j<indicatorPaths.lenght; j++) {
            indicatorPath = indicatorPaths[j];

            indicatorPathType = indicatorPath.type;
            indicatorPath.type = indicatorPathType.toUpperCase();
            indicatorPathSource = indicatorPath.source;
            indicatorPath.source = indicatorPathSource.toUpperCase();
        }

        db.indicator.save(indicator);
    }
    print("Uppercase enum values for Indicator types and width");
    print("Uppercase enum values for Indicator path types and source");
}

function addHeightInIndicators() {
    indicators = db.indicator.find().map(function (indicators) {
        return indicators;
    });
    for(var i=0; i<indicators.length; i++) {
        indicator = indicators[i];

        if(indicator.type == "chart" || indicator.type == "CHART") {
            indicator['height'] = "MEDIUM";
        } else {
            indicator['height'] = "SMALL";
        }
        db.indicator.save(indicator);
    }
    print("Height field added in Indicators");
}

function addVisibility() {
    stakeholders = db.stakeholder.find().map(function (stakeholders) {
        return stakeholders;
    });
    for (var i = 0; i < stakeholders.length; i++) {
        stakeholder = stakeholders[i];

        if (stakeholder.isActive) {
            if (stakeholder.isPublic) {
                stakeholder['visibility'] = "PUBLIC";
            } else {
                stakeholder['visibility'] = "RESTRICTED";
            }
        } else {
            stakeholder['visibility'] = "PRIVATE";
        }

        db.stakeholder.save(stakeholder);
    }
    print("Add visibility field for Stakeholders");

    topics = db.topic.find().map(function (topics) {
        return topics;
    });
    for (var i = 0; i < topics.length; i++) {
        topic = topics[i];

        topicVisibility = "PRIVATE";
        if (topic.isActive) {
            if (topic.isPublic) {
                topicVisibility = "PUBLIC";
            } else {
                topicVisibility = "RESTRICTED";
            }
        }

        topic['visibility'] = topicVisibility;
        db.topic.save(topic);
    }
    print("Add visibility field for Topics");

    categories = db.category.find().map(function (categories) {
        return categories;
    });
    for (var i = 0; i < categories.length; i++) {
        category = categories[i];

        if (category.isActive) {
            if (category.isPublic) {
                category['visibility'] = "PUBLIC";
            } else {
                category['visibility'] = "RESTRICTED";
            }
        } else {
            category['visibility'] = "PRIVATE";
        }

        db.category.save(category);
    }
    print("Add visibility field for Categories");

    subCategories = db.subCategory.find().map(function (subCategories) {
        return subCategories;
    });
    for (var i = 0; i < subCategories.length; i++) {
        subCategory = subCategories[i];

        if (subCategory.isActive) {
            if (subCategory.isPublic) {
                subCategory['visibility'] = "PUBLIC";
            } else {
                subCategory['visibility'] = "RESTRICTED";
            }
        } else {
            subCategory['visibility'] = "PRIVATE";
        }

        db.subCategory.save(subCategory);
    }
    print("Add visibility field for SubCategories");

    indicators = db.indicator.find().map(function (indicators) {
        return indicators;
    });
    for (var i = 0; i < indicators.length; i++) {
        indicator = indicators[i];

        if (indicator.isActive) {
            if (indicator.isPublic) {
                indicator['visibility'] = "PUBLIC";
            } else {
                indicator['visibility'] = "RESTRICTED";
            }
        } else {
            indicator['visibility'] = "PRIVATE";
        }

        db.indicator.save(indicator);
    }
    print("Add visibility field for Indicators");
}

function removeIsActiveAndIsPublic() {
    stakeholders = db.stakeholder.find().map(function (stakeholders) {
        return stakeholders;
    });
    for(var i=0; i<stakeholders.length; i++) {
        stakeholder = stakeholders[i];

        db.stakeholder.update({"_id": stakeholder._id}, {$unset: {isPublic: "", isActive: ""}});
    }

    topics = db.topic.find().map(function (topics) {
        return topics;
    });
    for(var i=0; i<topics.length; i++) {
        topic = topics[i];

        db.topic.update({"_id": topic._id}, {$unset: {isPublic: "", isActive: ""}});
    }

    categories = db.category.find().map(function (categories) {
        return categories;
    });
    for(var i=0; i<categories.length; i++) {
        category = categories[i];

        db.category.update({"_id": category._id}, {$unset: {isPublic: "", isActive: ""}});
    }

    subCategories = db.subCategory.find().map(function (subCategories) {
        return subCategories;
    });
    for(var i=0; i<subCategories.length; i++) {
        subCategory = subCategories[i];

        db.subCategory.update({"_id": subCategory._id}, {$unset: {isPublic: "", isActive: ""}});
    }

    indicators = db.indicator.find().map(function (indicators) {
        return indicators;
    });
    for(var i=0; i<indicators.length; i++) {
        indicator = indicators[i];

        db.indicator.update({"_id": indicator._id}, {$unset: {isPublic: "", isActive: ""}});
    }
}

function addAdminToolsCollections () {
    db.createCollection("portal");
    db.createCollection("entity");
    db.createCollection("page");
    db.createCollection("pageHelpContent");
    db.createCollection("divId");
    db.createCollection("divHelpContent");
}

function addPortals() {
    db.portal.save({"pid": "monitor", "name": "Monitor", "type": "monitor", "piwik": null, "pages": {}, "entities": {}});
}

function uniqueIndexes() {
    db.portal.createIndex( { "pid": 1 }, { unique: true } );
    db.stakeholder.createIndex( { "alias": 1 }, { unique: true } );
}

upperCaseEnumValues();
addHeightInIndicators();
addVisibility();
removeIsActiveAndIsPublic();
addAdminToolsCollections();
addPortals();
uniqueIndexes();
