function statsProfileOfIndicatorsAsVariable() {
  print("statsProfileOfIndicatorsAsVariable");

  numOfNumbers = 0;
  numOfCharts = 0;
  numOfNonMonitorProfiles = 0;
  numOfMonitorProfiles = 0;
  numOfNoProfiles = 0;
  differentProfiles = new Set();

  // indicators = db.indicator.find({"type": "chart"}).map(function (indicator) {
  // indicators = db.indicator.find({"type": "number"}).map(function (indicator) {
  indicators = db.indicator.find().map(function (indicator) {
    return indicator;
  });

  print(indicators.length);

  for (var i = 0; i < indicators.length; i++) {
    indicator = indicators[i];

    indicatorPaths = indicator.indicatorPaths;
    if(indicatorPaths) {
      for (var j = 0; j < indicatorPaths.length; j++) {
        indicatorPath = indicatorPaths[j];
        chartObjectStr = "";
        // if(indicator.type == "chart") {
          chartObjectStr = indicatorPath.chartObject;
          // chartObject = JSON.parse(chartObjectStr);
        if(indicator.type == "chart") {
          numOfCharts++;
        } else {
          numOfNumbers++;
        }

        // if(i==0) {
          // if(chartObject.chartDescription != null && chartObject.chartDescription.queries != null) {
          //   print(chartObject.chartDescription.queries.length);
          //   for(var z = 0; z < chartObject.chartDescription.queries.length; z++) {
          //     query = chartObject.chartDescription.queries[z].query;
          //     print(query.profile);
          //     query.profile = "((__statsProfile__))";
          //   }
          // }
          // indicatorPath.chartObject = JSON.stringify(chartObject);

        if(chartObjectStr != null) {
          var included = chartObjectStr.includes('"profile":"monitor"');
          if (!included) {
            numOfNonMonitorProfiles++;
            print("Indicator with id: " + indicator._id + " has not monitor profile.");
          } else {
            numOfMonitorProfiles++;
          }

          splitted = chartObjectStr.split('"profile":"');
          if (splitted.length == 1) {
            numOfNoProfiles++;
          }
          for (var z = 1; z < splitted.length; z = z + 2) {
            prof = splitted[z].split('"')[0];
            differentProfiles.add(prof);
          }

          chartObjectStr = chartObjectStr.split('"profile":"monitor"').join('"profile":"((__profile__))"');
          chartObjectStr = chartObjectStr.split('"profile":"OpenAIRE All-inclusive"').join('"profile":"((__profile__))"');
          chartObjectStr = chartObjectStr.split('"profile":"OpenAIRE Monitor"').join('"profile":"((__profile__))"');
          indicatorPath.chartObject = chartObjectStr;
        } else {
          print("Indicator with id: " + indicator._id + " has no chartObject");
        }
      }
    }

    // save indicator
    db.indicator.save(indicator);
  }
  print("\n");
  print("numOfNumbers: "+numOfNumbers);
  print("numOfCharts: "+numOfCharts);
  print("numOfMonitorProfiles: "+numOfMonitorProfiles);
  print("numOfNonMonitorProfiles: "+numOfNonMonitorProfiles);
  print("numOfNoProfiles: "+numOfNoProfiles);
  print("Different profiles are: ");
  for (var item of differentProfiles) {
    print(item);
  }
}

function addFundingLevelInFilters(filter) {
    if(filter.groupFilters && filter.groupFilters.length > 0) {
        var index = filter.groupFilters.findIndex(filter => filter.field.includes('project'));
        if(index !== -1) {
            print('before: ' + JSON.stringify(filter));
            var prefix = filter.groupFilters[index].field.substring(0, filter.groupFilters[index].field.indexOf('project'));
            if(!filter.groupFilters.find(filter => filter.field === prefix + "project.funding level 1")) {
              filter.groupFilters.push({
                "field": prefix + "project.funding level 1",
                "type": "contains",
                "values": [
                  '((__index_shortName__))'
                ]
              });
              print('after: ' + JSON.stringify(filter));
            } else {
              print('Already added');
            }
        }
        return filter;
    }
}

function addFundingStreamInDefaultMSCA() {
  print("addFundingStreamInDefaultMSCA")

  var stakeholder = db.stakeholder.findOne({"alias": "default-fl1"});
  if(stakeholder) {
    stakeholder.topics.forEach((topic) => {
      var topicObj = db.topic.findOne({"_id": ObjectId(topic)});
      topicObj.categories.forEach((category) => {
        var categoryObj = db.category.findOne({"_id": ObjectId(category)});
        categoryObj.subCategories.forEach((subCategory) => {
          var subCategoryObj = db.subCategory.findOne({"_id": ObjectId(subCategory)});
          subCategoryObj.numbers.forEach((number) => {
            var section = db.section.findOne({"_id": ObjectId(number)});
            section.indicators.forEach((indicator) => {
              var indicatorObject = db.indicator.findOne({"_id": ObjectId(indicator)});
              if(indicatorObject.indicatorPaths[0].parameters) {
                indicatorObject.indicatorPaths[0].parameters['index_shortName'] = stakeholder.index_shortName.toLowerCase();
                if(indicatorObject.indicatorPaths[0] && indicatorObject.indicatorPaths[0].chartObject) {
                  var json = JSON.parse(indicatorObject.indicatorPaths[0].chartObject);
                  if(json.series && json.series.length > 0) {
                    json.series.forEach(query => {
                      if(query.query && query.query.filters && query.query.filters.length > 0) {
                        query.query.filters.forEach(filter => {
                          filter = addFundingLevelInFilters(filter);
                        });
                      }
                    });
                    indicatorObject.indicatorPaths[0].chartObject = JSON.stringify(json);
                    db.indicator.save(indicatorObject);
                  }
                }
              }
            });
          });
          subCategoryObj.charts.forEach((chart) => {
            var section = db.section.findOne({"_id": ObjectId(chart)});
            section.indicators.forEach((indicator) => {
              var indicatorObject = db.indicator.findOne({"_id": ObjectId(indicator)});
              if(indicatorObject.indicatorPaths[0].parameters) {
                  indicatorObject.indicatorPaths[0].parameters['index_shortName'] = stakeholder.index_shortName.toLowerCase();
                  if (indicatorObject.indicatorPaths[0] && indicatorObject.indicatorPaths[0].chartObject) {
                      var json = JSON.parse(indicatorObject.indicatorPaths[0].chartObject);
                      if (json.chartDescription && json.chartDescription.queries && json.chartDescription.queries.length > 0) {
                          json.chartDescription.queries.forEach(query => {
                              if (query.query && query.query.filters && query.query.filters.length > 0) {
                                  query.query.filters.forEach(filter => {
                                      filter = addFundingLevelInFilters(filter);
                                  });
                              }
                          });
                          indicatorObject.indicatorPaths[0].chartObject = JSON.stringify(json);
                          db.indicator.save(indicatorObject);
                      }
                  }
              }
            });
          });
        });

      });
    });
  } else {
    print("Profile doesn't exist")
  }
}

statsProfileOfIndicatorsAsVariable();
addFundingStreamInDefaultMSCA();
