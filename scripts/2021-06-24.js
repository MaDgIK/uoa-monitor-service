function addHomePageInPortalType(portalType) {
    homePageID = db.page.insertOne({ "route" : "/", "name" : "Home", "type" : "other", "entities" : [ ], "portalType" : portalType, "top" : false, "bottom" : false, "left" : false, "right" : false }).insertedId.str;
    print("Creating Home page with id " + homePageID);
    portals = db.portal.find({"type": portalType}).map( function(portal) { return portal; } );
    for (var i = 0; i < portals.length; i++) {
        portal_pages = portals[i].pages;

        portal_pages[homePageID] = true;

        portal_pid = portals[i].pid;
        db.portal.update({ "pid" : portal_pid },{$set: { "pages": portal_pages}});
        print("Add home page with id " + homePageID + " on " + portalType + " " + portal_pid);
    }
}

function addFooterDivIdForPortalType(portalType) {
    homePageID = db.page.find({"portalType": portalType, "route": "/"}).map( function(portal) { return portal._id.str; } ).toString();
    db.divId.save({ "name" : "footer", "pages" : [ homePageID ], "portalType" : portalType });
    print("divId 'footer' added for "+portalType+" (home page id: "+homePageID + ")");
}

function addFooterHelpTextForPortalType(portalType) {
    footerDivIdID = db.divId.find({"portalType": portalType, "name": "footer"}).map( function(divId) { return divId._id.str; } ).toString();

    portals = db.portal.find({"type": portalType}).map( function(portal) { return portal; } );
    for (var j = 0; j < portals.length; j++) {
        portal = portals[j];

        var content = "This OpenAIRE MONITOR dashboard is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541 and 101017452";

        if(portal.pid == "egi") {
            content = "This OpenAIRE MONITOR dashboard is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541 and 101017452";

        } else if(portal.pid == "risis") {

            content = "This OpenAIRE MONITOR dashboard is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541 and 101017452 and 824091";

        } else if(portal.pid == "sobigdata") {

            content = "This OpenAIRE MONITOR dashboard is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541 and 101017452 and 871042";
        }

        db.divHelpContent.save(
            { "divId" : footerDivIdID,
                "portal" : portal._id.str,
                "content" : "<p class=\"uk-margin-remove-bottom\"><span style=\"font-size:8pt\">"+content+"</span></p>",
                "isActive" : true
            }
        );

        print("div help text for divId 'footer' added for "+portalType +" (id: "+ portal._id + " - pid: " + portal.pid + " - footer divId id: "+footerDivIdID + ")");
    }
}

addHomePageInPortalType("funder");
addFooterDivIdForPortalType("funder");
addFooterHelpTextForPortalType("funder");
addHomePageInPortalType("ri");
addFooterDivIdForPortalType("ri");
addFooterHelpTextForPortalType("ri");
addHomePageInPortalType("organization");
addFooterDivIdForPortalType("organization");
addFooterHelpTextForPortalType("organization");
