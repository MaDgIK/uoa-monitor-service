package eu.dnetlib.uoamonitorservice.dto;

import eu.dnetlib.uoamonitorservice.entities.Indicator;
import eu.dnetlib.uoamonitorservice.entities.Section;
import eu.dnetlib.uoamonitorservice.generics.SectionGeneric;
import io.swagger.models.auth.In;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class SectionFull extends SectionGeneric<Indicator> {
    public SectionFull() {}

    public SectionFull(Section section, List<Indicator> indicators) {
        super(section);
        indicators.removeIf(Objects::isNull);
        this.indicators = indicators;
    }

    public Optional<Indicator> getIndicatorByDefaultId(String id) {
       return this.indicators.stream().filter(indicator -> indicator.getDefaultId().equals(id)).findFirst();
    }

    public void removeIndicator(String id) {
        this.indicators.removeIf(indicator -> indicator.getId().equals(id));
    }

    public void addIndicator(Indicator indicator) {
        this.indicators.add(indicator);
    }
}
