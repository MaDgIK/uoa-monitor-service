package eu.dnetlib.uoamonitorservice.dto;

import eu.dnetlib.uoamonitorservice.entities.Stakeholder;

import java.util.List;

public class ManageStakeholders {
    List<Stakeholder> templates;
    List<Stakeholder> standalone;
    List<Stakeholder> dependent;
    List<Stakeholder> umbrella;

    public ManageStakeholders() {
    }

    public List<Stakeholder> getTemplates() {
        return templates;
    }

    public void setTemplates(List<Stakeholder> templates) {
        this.templates = templates;
    }

    public List<Stakeholder> getStandalone() {
        return standalone;
    }

    public void setStandalone(List<Stakeholder> standalone) {
        this.standalone = standalone;
    }

    public List<Stakeholder> getDependent() {
        return dependent;
    }

    public void setDependent(List<Stakeholder> dependent) {
        this.dependent = dependent;
    }

    public List<Stakeholder> getUmbrella() {
        return umbrella;
    }

    public void setUmbrella(List<Stakeholder> umbrella) {
        this.umbrella = umbrella;
    }
}
