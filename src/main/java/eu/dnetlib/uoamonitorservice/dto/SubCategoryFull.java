package eu.dnetlib.uoamonitorservice.dto;

import eu.dnetlib.uoamonitorservice.generics.SubCategoryGeneric;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class SubCategoryFull extends SubCategoryGeneric<SectionFull> {
    public SubCategoryFull() {}

    public SubCategoryFull(SubCategoryGeneric subCategory, List<SectionFull> numbers, List<SectionFull> charts) {
        super(subCategory);
        numbers.removeIf(Objects::isNull);
        charts.removeIf(Objects::isNull);
        this.numbers = numbers;
        this.charts = charts;
    }

    public Optional<SectionFull> getSectionByDefaultId(String id) {
        List<SectionFull> sections = new ArrayList<>(this.numbers);
        sections.addAll(this.charts);
        return sections.stream().filter(section -> section.getDefaultId().equals(id)).findFirst();
    }
}
