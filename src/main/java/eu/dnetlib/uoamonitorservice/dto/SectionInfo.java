package eu.dnetlib.uoamonitorservice.dto;

import java.util.List;

public class SectionInfo {
    private String id;
    private List<String> indicators;

    public SectionInfo() {
    }

    public SectionInfo(String id, List<String> indicators) {
        this.id = id;
        this.indicators = indicators;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<String> indicators) {
        this.indicators = indicators;
    }
}
