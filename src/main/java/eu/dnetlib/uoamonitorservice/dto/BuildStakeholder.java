package eu.dnetlib.uoamonitorservice.dto;

import eu.dnetlib.uoamonitorservice.entities.Stakeholder;

public class BuildStakeholder {
    private Stakeholder stakeholder;
    private String copyId;
    private boolean umbrella = false;
    private boolean standalone = true;

    public BuildStakeholder() {
    }

    public Stakeholder getStakeholder() {
        return stakeholder;
    }

    public void setStakeholder(Stakeholder stakeholder) {
        this.stakeholder = stakeholder;
    }

    public String getCopyId() {
        return copyId;
    }

    public void setCopyId(String copyId) {
        this.copyId = copyId;
    }

    public boolean isUmbrella() {
        return umbrella;
    }

    public void setUmbrella(boolean umbrella) {
        this.umbrella = umbrella;
    }

    public boolean isStandalone() {
        return standalone;
    }

    public void setStandalone(boolean standalone) {
        this.standalone = standalone;
    }
}
