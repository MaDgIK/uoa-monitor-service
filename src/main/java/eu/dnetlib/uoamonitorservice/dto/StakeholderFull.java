package eu.dnetlib.uoamonitorservice.dto;

import eu.dnetlib.uoamonitorservice.entities.Stakeholder;
import eu.dnetlib.uoamonitorservice.generics.StakeholderGeneric;
import eu.dnetlib.uoamonitorservice.primitives.Umbrella;

import java.util.List;
import java.util.Objects;

public class StakeholderFull extends StakeholderGeneric<TopicFull, Stakeholder> {
    public List<Stakeholder> otherParents;
    public Stakeholder parent;

    public StakeholderFull() {
        super();
    }

    public StakeholderFull(StakeholderGeneric stakeholder, List<TopicFull> topics, Umbrella<Stakeholder> umbrella) {
        super(stakeholder);
        topics.removeIf(Objects::isNull);
        this.topics = topics;
        this.setUmbrella(umbrella);
    }

    public List<Stakeholder> getOtherParents() {
        return otherParents;
    }

    public void setOtherParents(List<Stakeholder> otherParents) {
        this.otherParents = otherParents;
    }

    public Stakeholder getParent() {
        return parent;
    }

    public void setParent(Stakeholder parent) {
        this.parent = parent;
    }
}
