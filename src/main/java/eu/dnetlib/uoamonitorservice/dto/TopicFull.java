package eu.dnetlib.uoamonitorservice.dto;

import eu.dnetlib.uoamonitorservice.generics.TopicGeneric;

import java.util.List;
import java.util.Objects;

public class TopicFull extends TopicGeneric<CategoryFull> {
    public TopicFull() {
        super();
    }

    public TopicFull(TopicGeneric topic, List<CategoryFull> categories) {
        super(topic);
        categories.removeIf(Objects::isNull);
        this.categories = categories;
    }
}
