package eu.dnetlib.uoamonitorservice.dto;

public class MoveIndicator {
    private String target;
    private SectionInfo from;
    private SectionInfo to;

    public MoveIndicator() {
    }

    public MoveIndicator(String target, SectionInfo from, SectionInfo to) {
        this.target = target;
        this.from = from;
        this.to = to;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public SectionInfo getFrom() {
        return from;
    }

    public void setFrom(SectionInfo from) {
        this.from = from;
    }

    public SectionInfo getTo() {
        return to;
    }

    public void setTo(SectionInfo to) {
        this.to = to;
    }
}
