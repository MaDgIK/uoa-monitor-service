package eu.dnetlib.uoamonitorservice.dto;

import eu.dnetlib.uoamonitorservice.generics.CategoryGeneric;

import java.util.List;
import java.util.Objects;

public class CategoryFull extends CategoryGeneric<SubCategoryFull> {
    public CategoryFull() {
        super();
    }

    public CategoryFull(CategoryGeneric category, List<SubCategoryFull> subCategories) {
        super(category);
        subCategories.removeIf(Objects::isNull);
        this.subCategories = subCategories;
    }
}
