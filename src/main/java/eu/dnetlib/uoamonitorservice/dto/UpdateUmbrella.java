package eu.dnetlib.uoamonitorservice.dto;

import eu.dnetlib.uoamonitorservice.primitives.Action;
import eu.dnetlib.uoamonitorservice.primitives.StakeholderType;

import java.util.List;

public class UpdateUmbrella {
    StakeholderType type;
    Action action;
    String child;
    List<StakeholderType> types;
    List<String> children;

    public UpdateUmbrella() {
    }

    public StakeholderType getType() {
        return type;
    }

    public void setType(StakeholderType type) {
        this.type = type;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public List<StakeholderType> getTypes() {
        return types;
    }

    public void setTypes(List<StakeholderType> types) {
        this.types = types;
    }

    public List<String> getChildren() {
        return children;
    }

    public void setChildren(List<String> children) {
        this.children = children;
    }
}
