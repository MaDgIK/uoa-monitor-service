package eu.dnetlib.uoamonitorservice.generics;

import eu.dnetlib.uoamonitorservice.primitives.IndicatorType;

import java.util.ArrayList;
import java.util.List;

public class SectionGeneric<T> extends Common {
    protected String title;
    protected String stakeholderAlias;
    protected IndicatorType type;
    protected List<T> indicators;

    public SectionGeneric() {
    }

    public SectionGeneric(SectionGeneric section) {
        id = section.getId();
        title = section.getTitle();
        defaultId = section.getDefaultId();
        stakeholderAlias = section.getStakeholderAlias();
        setType(section.getType());
        creationDate = section.getCreationDate();
        updateDate = section.getUpdateDate();
        indicators = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStakeholderAlias() {
        return stakeholderAlias;
    }

    public void setStakeholderAlias(String stakeholderAlias) {
        this.stakeholderAlias = stakeholderAlias;
    }

    public String getType() {
        if (type == null) {
            return null;
        }
        return type.name();
    }

    public void setType(String type) {
        if (type == null) {
            this.type = null;
        } else {
            this.type = IndicatorType.valueOf(type);
        }
    }

    public boolean isNumber() {
        return this.type == IndicatorType.NUMBER || this.type == IndicatorType.number;
    }

    public List<T> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<T> indicators) {
        this.indicators = indicators;
    }
}
