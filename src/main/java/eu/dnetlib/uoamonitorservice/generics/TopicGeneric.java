package eu.dnetlib.uoamonitorservice.generics;

import java.util.ArrayList;
import java.util.List;

public class TopicGeneric<T> extends Common {
    protected String icon;
    protected List<T> categories;

    public TopicGeneric() {
    }

    public TopicGeneric(TopicGeneric topic) {
        id = topic.getId();
        name = topic.getName();
        alias = topic.getAlias();
        description = topic.getDescription();
        icon = topic.getIcon();
        setVisibility(topic.getVisibility());
        creationDate = topic.getCreationDate();
        updateDate = topic.getUpdateDate();
        defaultId = topic.getDefaultId();
        categories = new ArrayList<>();
    }


    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<T> getCategories() {
        return categories;
    }

    public void setCategories(List<T> categories) {
        this.categories = categories;
    }
}
