package eu.dnetlib.uoamonitorservice.generics;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
public class CategoryGeneric<T> extends Common {
    protected boolean isOverview;
    protected List<T> subCategories;

    public CategoryGeneric() {}

    public CategoryGeneric(CategoryGeneric category) {
        id = category.getId();
        name = category.getName();
        alias = category.getAlias();
        description = category.getDescription();
        setVisibility(category.getVisibility());
        creationDate = category.getCreationDate();
        updateDate = category.getUpdateDate();
        defaultId = category.getDefaultId();
        isOverview = category.getIsOverview();
        subCategories = new ArrayList<>();
    }

    public boolean getIsOverview() {
        return isOverview;
    }

    public void setIsOverview(boolean isOverview) {
        this.isOverview = isOverview;
    }

    public List<T> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<T> subCategories) {
        this.subCategories = subCategories;
    }
}
