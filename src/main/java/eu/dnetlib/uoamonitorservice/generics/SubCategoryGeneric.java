package eu.dnetlib.uoamonitorservice.generics;

import java.util.ArrayList;
import java.util.List;

public class SubCategoryGeneric<T> extends Common {
    protected List<T> charts;
    protected List<T> numbers;

    public SubCategoryGeneric() {}
    public SubCategoryGeneric(SubCategoryGeneric subCategory) {
        id = subCategory.getId();
        name = subCategory.getName();
        alias = subCategory.getAlias();
        description = subCategory.getDescription();
        setVisibility(subCategory.getVisibility());
        creationDate = subCategory.getCreationDate();
        updateDate = subCategory.getUpdateDate();
        defaultId = subCategory.getDefaultId();
        numbers = new ArrayList<>();
        charts = new ArrayList<>();
    }

    public List<T> getCharts() {
        return charts;
    }

    public void setCharts(List<T> charts) {
        this.charts = charts;
    }

    public List<T> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<T> numbers) {
        this.numbers = numbers;
    }
}
