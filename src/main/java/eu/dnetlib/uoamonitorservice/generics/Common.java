package eu.dnetlib.uoamonitorservice.generics;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.dnetlib.uoamonitorservice.primitives.Visibility;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

import java.util.Date;

public class Common {
    @Id
    @JsonProperty("_id")
    protected String id;
    @CreatedDate
    protected Date creationDate;
    @LastModifiedDate
    protected Date updateDate;
    protected String defaultId;
    protected String name;
    protected String description;
    protected String alias;
    protected Visibility visibility = Visibility.PRIVATE;

    public void update(Common common) {
        this.id = common.getId();
        this.creationDate = common.getCreationDate();
        this.updateDate = common.getUpdateDate();
    }

    public Common override(Common common, Common old) {
        if(this.getName() != null && !this.getName().equals(common.getName()) &&
                (old.getName() == null || old.getName().equals(common.getName()))) {
            common.setName(this.getName());
            common.setAlias(this.getAlias());
        }
        if(this.getDescription() != null && !this.getDescription().equals(common.getDescription()) &&
                (old.getDescription() == null || old.getDescription().equals(common.getDescription()))) {
            common.setDescription(this.getDescription());
        }
        return common;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDefaultId() {
        return defaultId;
    }

    public void setDefaultId(String defaultId) {
        this.defaultId = defaultId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }
}
