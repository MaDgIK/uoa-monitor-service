package eu.dnetlib.uoamonitorservice.generics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.dnetlib.uoamonitorservice.primitives.Locale;
import eu.dnetlib.uoamonitorservice.primitives.StakeholderType;
import eu.dnetlib.uoamonitorservice.primitives.Umbrella;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class StakeholderGeneric<T, S> extends Common {
    protected StakeholderType type;
    protected Date projectUpdateDate = null;
    protected String index_id;
    protected String index_name;
    protected String index_shortName;
    protected String statsProfile = "monitor";
    protected String logoUrl;
    protected boolean isUpload = false;
    protected Locale locale = Locale.EU;
    protected String funderType;
    protected Boolean standalone = true;
    protected Boolean copy;
    protected List<T> topics = new ArrayList<>();
    protected Umbrella<S> umbrella;

    public StakeholderGeneric() {
    }

    public StakeholderGeneric(StakeholderGeneric stakeholder) {
        id = stakeholder.getId();
        setType(stakeholder.getType());
        projectUpdateDate = stakeholder.getProjectUpdateDate();
        index_id = stakeholder.getIndex_id();
        index_name = stakeholder.getIndex_name();
        index_shortName = stakeholder.getIndex_shortName();
        statsProfile = stakeholder.getStatsProfile();
        logoUrl = stakeholder.getLogoUrl();
        isUpload = stakeholder.getIsUpload();
        name = stakeholder.getName();
        alias = stakeholder.getAlias();
        description = stakeholder.getDescription();
        defaultId = stakeholder.getDefaultId();
        setLocale(stakeholder.getLocale());
        setVisibility(stakeholder.getVisibility());
        this.funderType = stakeholder.getFunderType();
        this.copy = stakeholder.isCopy();
        this.standalone = stakeholder.isStandalone();
        creationDate = stakeholder.getCreationDate();
        updateDate = stakeholder.getUpdateDate();
    }

    public String getType() {
        if (type == null) {
            return null;
        }
        return type.name();
    }

    public void setType(String type) {
        this.type = StakeholderType.convert(type);
    }

    public Date getProjectUpdateDate() {
        return projectUpdateDate;
    }

    public void setProjectUpdateDate(Date projectUpdateDate) {
        this.projectUpdateDate = projectUpdateDate;
    }

    public String getIndex_id() {
        return index_id;
    }

    public void setIndex_id(String index_id) {
        this.index_id = index_id;
    }

    public String getIndex_name() {
        return index_name;
    }

    public void setIndex_name(String index_name) {
        this.index_name = index_name;
    }

    public String getIndex_shortName() {
        return index_shortName;
    }

    public void setIndex_shortName(String index_shortName) {
        this.index_shortName = index_shortName;
    }

    public String getStatsProfile() {
        return statsProfile;
    }

    public void setStatsProfile(String statsProfile) {
        this.statsProfile = statsProfile;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public boolean getIsUpload() {
        return isUpload;
    }

    public void setIsUpload(boolean isUpload) {
        this.isUpload = isUpload;
    }

    public String getLocale() {
        return locale.label;
    }

    public void setLocale(String label) {
        Locale locale = null;
        for (Locale l : Locale.values()) {
            if (l.label.equals(label)) {
                locale = l;
            }
        }
        this.locale = locale;
    }

    public String getFunderType() {
        return funderType;
    }

    public void setFunderType(String funderType) {
        this.funderType = funderType;
    }

    public boolean isStandalone() {
        if(standalone == null) {
            standalone = true;
        }
        return standalone;
    }

    public void setStandalone(boolean standalone) {
        this.standalone = standalone;
    }

    public boolean isCopy() {
        if(copy == null) {
            copy = this.defaultId != null;
        }
        return copy;
    }

    public void setCopy(boolean copy) {
        this.copy = copy;
    }

    public List<T> getTopics() {
        return topics;
    }

    public void setTopics(List<T> topics) {
        this.topics = topics;
    }

    public Umbrella<S> getUmbrella() {
        return umbrella;
    }

    @JsonIgnore
    public Optional<Umbrella<S>> getUmbrellaOptional() {
        return Optional.ofNullable(umbrella);
    }

    public void setUmbrella(Umbrella<S> umbrella) {
        this.umbrella =umbrella;
    }
}
