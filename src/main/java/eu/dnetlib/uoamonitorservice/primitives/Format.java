package eu.dnetlib.uoamonitorservice.primitives;

public enum Format {
    NUMBER, PERCENTAGE
}
