package eu.dnetlib.uoamonitorservice.primitives;

import eu.dnetlib.uoamonitorservice.entities.Stakeholder;
import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.service.StakeholderService;

import java.util.*;
import java.util.stream.Collectors;

public class Umbrella<T> {
    List<StakeholderType> types;
    Map<StakeholderType, List<T>> children;

    public Umbrella() {
        this.types = new ArrayList<>();
        this.children = new HashMap<>();
    }

    public Umbrella(List<StakeholderType> types) {
        this.types = types;
        this.children = new HashMap<>();
    }

    public static Umbrella<String> convert(Umbrella<Stakeholder> umbrellaFull) {
        if(umbrellaFull == null) {
            return null;
        }
        Umbrella<String> umbrella = new Umbrella<>(umbrellaFull.getTypes());
        umbrella.types.forEach(type -> umbrella.children.put(type, umbrellaFull.getChildren().get(type).stream().map(Common::getId).collect(Collectors.toList())));
        return umbrella;
    }

    public static Umbrella<Stakeholder> convert(Umbrella<String> umbrella, StakeholderService service) {
        if(umbrella == null) {
            return null;
        }
        Umbrella<Stakeholder> umbrellaFull = new Umbrella<>(umbrella.getTypes());
        umbrella.types.forEach(type -> umbrellaFull.children.put(type, umbrella.getChildren().get(type).stream().map(service::findByPath).collect(Collectors.toList())));
        return umbrellaFull;
    }

    public Map<StakeholderType, List<T>> getChildren() {
        return children;
    }

    public void setChildren(Map<StakeholderType, List<T>> children) {
        this.children = children;
    }

    public List<StakeholderType> getTypes() {
        return types;
    }

    public void setTypes(List<StakeholderType> types) {
        this.types = types;
    }

    public boolean addType(StakeholderType type) {
        if(!this.types.contains(type)) {
            this.types.add(type);
            this.children.put(type, new ArrayList<>());
            return true;
        }
        return false;
    }

    public boolean removeType(StakeholderType type) {
        if(this.types.contains(type)) {
            this.types.remove(type);
            this.children.remove(type);
            return true;
        }
        return false;
    }

    public boolean addChild(StakeholderType type, T child) {
        if(this.types.contains(type)) {
            if(!this.children.containsKey(type)) {
                this.children.put(type, new ArrayList<>());
            }
            if(!this.children.get(type).contains(child)) {
                return this.children.get(type).add(child);
            }
        }
        return false;
    }

    public boolean removeChild(StakeholderType type, T child) {
        if(this.types.contains(type)) {
            if(!this.children.containsKey(type)) {
                return false;
            }
            if(this.children.get(type).contains(child)) {
                return this.children.get(type).remove(child);
            }
        }
        return false;
    }

    public boolean update(List<StakeholderType> types) {
        if(types == null) {
            return  false;
        } else if(types.size() != this.types.size()) {
            return false;
        } else {
            if(new HashSet<>(this.types).containsAll(types)) {
                this.types = types;
                return true;
            }
            return false;
        }
    }

    public boolean update(StakeholderType type, List<T> children) {
        if(children == null) {
            return  false;
        } else if(this.children.get(type) == null) {
            return false;
        } else if(children.size() != this.children.get(type).size()) {
            return false;
        } else {
            return new HashSet<>(this.children.get(type)).containsAll(children);
        }
    }
}
