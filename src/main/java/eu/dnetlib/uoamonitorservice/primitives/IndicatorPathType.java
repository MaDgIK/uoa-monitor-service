package eu.dnetlib.uoamonitorservice.primitives;

public enum IndicatorPathType {
    // Do not rename or remove existing values. This may cause problems with already stored values in DB
    table, bar, column, pie, line, other,
    TABLE, BAR, COLUMN, PIE, LINE, OTHER;
}
