package eu.dnetlib.uoamonitorservice.primitives;

public enum Action {
    ADD, REMOVE, UPDATE
}
