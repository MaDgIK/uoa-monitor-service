package eu.dnetlib.uoamonitorservice.primitives;

public enum StakeholderType {
    // Do not rename or remove existing values. This may cause problems with already stored values in DB
    funder, ri, project, organization,
    country, researcher, datasource,
    publisher, journal,
    FUNDER, RI, PROJECT, ORGANIZATION,
    COUNTRY, RESEARCHER, DATASOURCE,
    PUBLISHER, JOURNAL;

    public static StakeholderType convert(String type) {
        if(type == null) {
            return null;
        } else {
            return StakeholderType.valueOf(type);
        }
    }
}
