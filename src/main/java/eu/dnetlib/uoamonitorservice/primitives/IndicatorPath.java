package eu.dnetlib.uoamonitorservice.primitives;

import java.util.List;
import java.util.Map;

public class IndicatorPath {
    private IndicatorPathType type;  // for charts is type of chart {table, bar, column, etc}
    private Format format = Format.NUMBER;  // for numbers is if number is percentage or not
    private String source; // for numbers is the service {statistics, search, metrics} for charts is the tool {stats-tool,old,metrics, image}
    private String url;
    private List<String> jsonPath;
    private String chartObject;
    private Map<String, String> parameters;
    private Map<String, String> filters;
    //private Map<String, Map<String, String>> filters;

    public IndicatorPath() {}

    public IndicatorPath(IndicatorPath indicatorPath) {
        setType(indicatorPath.getType());
        setFormat(indicatorPath.getFormat());
        source = indicatorPath.getSource();
        url = indicatorPath.getUrl();
        jsonPath = indicatorPath.getJsonPath();
        chartObject = indicatorPath.getChartObject();
        parameters = indicatorPath.getParameters();
        filters = indicatorPath.getFilters();
    }

    public String getType() {
        if(type == null) {
            return null;
        }
        return type.name();
    }

//    public IndicatorPathType getType() {
//        return type;
//    }

    public void setType(String type) {
        if(type == null) {
            this.type = null;
        } else {
            this.type = IndicatorPathType.valueOf(type);
        }
    }

    public String getFormat() {
        if(format == null) {
            return null;
        }
        return format.name();
    }

    public void setFormat(String format) {
        if(format == null) {
            this.format = null;
        } else {
            this.format = Format.valueOf(format);
        }
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getJsonPath() {
        return jsonPath;
    }

    public void setJsonPath(List<String> jsonPath) {
        this.jsonPath = jsonPath;
    }

    public String getChartObject() {
        return chartObject;
    }

    public void setChartObject(String chartObject) {
        this.chartObject = chartObject;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public Map<String, String> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, String> filters) {
        this.filters = filters;
    }
}
