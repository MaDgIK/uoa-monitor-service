package eu.dnetlib.uoamonitorservice.primitives;

public enum IndicatorType {
    // Do not rename or remove existing values. This may cause problems with already stored values in DB
    number, chart,
    NUMBER, CHART;
}
