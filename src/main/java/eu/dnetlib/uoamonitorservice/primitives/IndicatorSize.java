package eu.dnetlib.uoamonitorservice.primitives;

public enum IndicatorSize {
    // Do not rename or remove existring values. This may cause problems with already stored values in DB
    small, medium, large,
    SMALL, MEDIUM, LARGE;
}
