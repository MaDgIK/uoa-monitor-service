package eu.dnetlib.uoamonitorservice.primitives;

public enum Locale {
    EN("en"), EU("eu");

    public final String label;

    Locale(String label) {
        this.label = label;
    }
}
