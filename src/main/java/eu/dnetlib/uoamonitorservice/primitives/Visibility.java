package eu.dnetlib.uoamonitorservice.primitives;

public enum Visibility {
    PUBLIC, RESTRICTED, PRIVATE
}
