package eu.dnetlib.uoamonitorservice.entities;

import eu.dnetlib.uoamonitorservice.dto.SubCategoryFull;
import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.generics.SubCategoryGeneric;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

@Document
public class SubCategory extends SubCategoryGeneric<String> {
    public SubCategory() {
        super();
    }

    public SubCategory(SubCategory subCategory) {
        super(subCategory);
    }

    public SubCategory(SubCategoryFull subCategory) {
        super(subCategory);
        this.numbers = subCategory.getNumbers().stream().map(Common::getId).collect(Collectors.toList());
        this.charts = subCategory.getCharts().stream().map(Common::getId).collect(Collectors.toList());
        this.numbers.removeIf(Objects::isNull);
        this.charts.removeIf(Objects::isNull);
    }

    public SubCategory copy() {
        SubCategory subCategory = new SubCategory(this);
        subCategory.setDefaultId(this.getId());
        subCategory.setId(null);
        return subCategory;
    }

    public SubCategory override(SubCategory subCategory, SubCategory old) {
        return (SubCategory) super.override(subCategory, old);
    }

    public void addNumber(String id, int index) {
        if(index == -1) {
            this.numbers.add(id);
        } else {
            this.numbers.add(index, id);
        }
    }

    public void removeNumber(String id) {
        this.numbers.remove(id);
    }

    public void addChart(String id, int index) {
        if(index == -1) {
            this.charts.add(id);
        } else {
            this.charts.add(index, id);
        }
    }

    public void removeChart(String id) {
        this.charts.remove(id);
    }
}
