package eu.dnetlib.uoamonitorservice.entities;

import eu.dnetlib.uoamonitorservice.dto.SectionFull;
import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.generics.SectionGeneric;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Document
public class Section extends SectionGeneric<String> {
    public Section() {}

    public Section(Section section) {
        super(section);
    }

    public Section(SectionFull section) {
        super(section);
        this.indicators = section.getIndicators().stream().map(Indicator::getId).collect(Collectors.toList());
        this.indicators.removeIf(Objects::isNull);
    }

    public Section copy() {
        Section section = new Section(this);
        section.setDefaultId(this.getId());
        section.setId(null);
        return section;
    }

    public Section override(Section section, Section old) {
        section = (Section) super.override(section, old);
        if(this.getTitle() != null && !this.getTitle().equals(section.getTitle()) &&
                (old.getTitle() == null || old.getTitle().equals(section.getTitle()))) {
            section.setTitle(this.getTitle());
        }
        return section;
    }

    public void addIndicator(String id) {
        this.indicators.add(id);
    }

    public void removeIndicator(String id) {
        this.indicators.remove(id);
    }
}
