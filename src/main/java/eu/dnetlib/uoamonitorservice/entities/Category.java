package eu.dnetlib.uoamonitorservice.entities;

import eu.dnetlib.uoamonitorservice.dto.CategoryFull;
import eu.dnetlib.uoamonitorservice.generics.CategoryGeneric;
import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.generics.TopicGeneric;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

@Document
public class Category extends CategoryGeneric<String> {
    public Category() {
        super();
    }

    public Category(Category category) {
        super(category);
    }

    public Category(CategoryFull category) {
        super(category);
        this.subCategories = category.getSubCategories().stream().map(Common::getId).collect(Collectors.toList());
        this.subCategories.removeIf(Objects::isNull);
    }

    public Category copy() {
        Category category = new Category(this);
        category.setDefaultId(this.getId());
        category.setId(null);
        return category;
    }

    public Category override(Category category, Category old) {
        return (Category) super.override(category, old);
    }

    public SubCategory createOverview() {
        SubCategory subCategory = new SubCategory();
        subCategory.setName("Overview");
        subCategory.setAlias("overview");
        subCategory.setVisibility(this.getVisibility());
        subCategory.setCharts(new ArrayList<>());
        subCategory.setNumbers(new ArrayList<>());
        return subCategory;
    }

    public void addSubCategory(String id) {
        this.subCategories.add(id);
    }

    public void removeSubCategory(String id) {
        this.subCategories.remove(id);
    }
}
