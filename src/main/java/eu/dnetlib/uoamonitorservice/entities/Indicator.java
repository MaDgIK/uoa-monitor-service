package eu.dnetlib.uoamonitorservice.entities;

import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.primitives.IndicatorPath;
import eu.dnetlib.uoamonitorservice.primitives.IndicatorSize;
import eu.dnetlib.uoamonitorservice.primitives.IndicatorType;

import java.util.List;

public class Indicator extends Common {
    private String additionalDescription;
    private IndicatorType type; //number,chart
    private IndicatorSize width; //small,medium,large
    private IndicatorSize height = IndicatorSize.MEDIUM; //small,medium,large
    private List<String> tags;  // this field is not used anywhere now
    private List<IndicatorPath> indicatorPaths;

    public Indicator() {}

    public Indicator(Indicator indicator) {
        id = indicator.getId();
        defaultId = indicator.getDefaultId();
        name = indicator.getName();
        description = indicator.getDescription();
        additionalDescription = indicator.getAdditionalDescription();
        creationDate = indicator.getCreationDate();
        updateDate = indicator.getUpdateDate();
        visibility = indicator.getVisibility();
        indicatorPaths = indicator.getIndicatorPaths();
        tags = indicator.getTags();
        setType(indicator.getType());
        width = indicator.getWidth();
        height = indicator.getHeight();
    }

    public Indicator copy() {
        Indicator indicator = new Indicator(this);
        indicator.setDefaultId(this.getId());
        indicator.setId(null);
        return indicator;
    }

    public Indicator override(Indicator indicator, Indicator old) {
        indicator = (Indicator) super.override(indicator, old);
        if(this.getAdditionalDescription() != null && !this.getAdditionalDescription().equals(indicator.getAdditionalDescription()) &&
                (old.getAdditionalDescription() == null || old.getAdditionalDescription().equals(indicator.getAdditionalDescription()))) {
            indicator.setAdditionalDescription(this.getAdditionalDescription());
        }
        if(this.getWidth() != null && !this.getWidth().equals(indicator.getWidth()) &&
                (old.getWidth() == null || old.getWidth().equals(indicator.getWidth()))) {
            indicator.setWidth(this.getWidth());
        }
        if(this.getHeight() != null && !this.getHeight().equals(indicator.getHeight()) &&
                (old.getHeight() == null || old.getHeight().equals(indicator.getHeight()))) {
            indicator.setHeight(this.getHeight());
        }
        indicator.setIndicatorPaths(this.getIndicatorPaths());
        return indicator;
    }

    public String getAdditionalDescription() {
        return additionalDescription;
    }

    public void setAdditionalDescription(String description) {
        this.additionalDescription = description;
    }

    public String getType() {
        if(type == null) {
            return null;
        }
        return type.name();
    }

    public void setType(String type) {
        if(type == null) {
            this.type = null;
        } else {
            this.type = IndicatorType.valueOf(type);
        }
    }

    public IndicatorSize getWidth() {
        return width;
    }

    public void setWidth(IndicatorSize width) {
        this.width = width;
    }

    public IndicatorSize getHeight() {
        return height;
    }

    public void setHeight(IndicatorSize height) {
        this.height = height;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<IndicatorPath> getIndicatorPaths() {
        return indicatorPaths;
    }

    public void setIndicatorPaths(List<IndicatorPath> indicatorPaths) {
        this.indicatorPaths = indicatorPaths;
    }
}
