package eu.dnetlib.uoamonitorservice.entities;

import eu.dnetlib.uoamonitorservice.dto.TopicFull;
import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.generics.TopicGeneric;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Document
public class Topic extends TopicGeneric<String> {

    public Topic() {
        super();
    }

    public Topic(Topic topic) {
        super(topic);
    }

    public Topic(TopicFull topic) {
        super(topic);
        this.categories = topic.getCategories().stream().map(Common::getId).collect(Collectors.toList());
        this.categories.removeIf(Objects::isNull);
    }

    public Topic(TopicFull topic, List<String> categories) {
        super(topic);
        this.categories = new ArrayList<>(categories);
    }

    public Topic copy() {
        Topic topic = new Topic(this);
        topic.setDefaultId(this.getId());
        topic.setId(null);
        return topic;
    }

    public Topic override(Topic topic, Topic old) {
        topic = (Topic) super.override(topic, old);
        if(this.getIcon() != null && !this.getIcon().equals(topic.getIcon()) &&
                (old.getIcon() == null || old.getIcon().equals(topic.getIcon()))) {
            topic.setIcon(this.getIcon());
        }
        return topic;
    }

    public void addCategory(String id) {
        this.categories.add(id);
    }

    public void removeCategory(String id) {
        this.categories.remove(id);
    }
}
