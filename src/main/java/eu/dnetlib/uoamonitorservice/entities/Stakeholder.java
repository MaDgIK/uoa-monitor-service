package eu.dnetlib.uoamonitorservice.entities;

import eu.dnetlib.uoamonitorservice.dto.StakeholderFull;
import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.generics.StakeholderGeneric;
import eu.dnetlib.uoamonitorservice.primitives.Umbrella;
import eu.dnetlib.uoamonitorservice.service.StakeholderService;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;
import java.util.stream.Collectors;

@Document
public class Stakeholder extends StakeholderGeneric<String, String> {

    public Stakeholder() {
        super();
    }

    public Stakeholder(Stakeholder stakeholder) {
        super(stakeholder);
    }

    public Stakeholder(StakeholderFull stakeholder, StakeholderService service) {
        super(stakeholder);
        Stakeholder old = service.findByPath(stakeholder.getId());
        this.defaultId = old.getDefaultId();
        this.standalone = old.isStandalone();
        this.topics = old.getTopics();
        this.umbrella = old.getUmbrella();
    }

    public void addTopic(String id) {
        this.topics.add(id);
    }

    public void removeTopic(String id) {
        this.topics.remove(id);
    }
}
