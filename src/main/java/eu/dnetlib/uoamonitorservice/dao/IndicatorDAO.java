package eu.dnetlib.uoamonitorservice.dao;

import eu.dnetlib.uoamonitorservice.entities.Indicator;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IndicatorDAO extends MongoRepository<Indicator, String> {
    List<Indicator> findAll();
    List<Indicator> findByDefaultId(String DefaultId);

    Optional<Indicator> findById(String Id);

    void delete(String Id);

    Indicator save(Indicator indicator);
    //List<Indicator> saveAll(List<Indicator> indicators);
}
