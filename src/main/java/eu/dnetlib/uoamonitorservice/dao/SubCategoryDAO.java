package eu.dnetlib.uoamonitorservice.dao;

import eu.dnetlib.uoamonitorservice.entities.SubCategory;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubCategoryDAO extends MongoRepository<SubCategory, String> {
    List<SubCategory> findAll();
    List<SubCategory> findByDefaultId(String defaultId);
    List<SubCategory> findByNumbersContaining(String id);
    List<SubCategory> findByChartsContaining(String id);

    Optional<SubCategory> findById(String Id);

    void delete(String Id);

    SubCategory save(SubCategory subCategory);
}
