package eu.dnetlib.uoamonitorservice.dao;

import eu.dnetlib.uoamonitorservice.entities.Section;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SectionDAO extends MongoRepository<Section, String> {
    List<Section> findAll();
    List<Section> findByDefaultId(String DefaultId);
    List<Section> findByIndicatorsContaining(String id);

    Optional<Section> findById(String Id);

    void delete(String Id);

    Section save(Section indicator);
}
