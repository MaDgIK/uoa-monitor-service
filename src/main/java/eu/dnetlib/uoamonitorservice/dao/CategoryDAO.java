package eu.dnetlib.uoamonitorservice.dao;

import eu.dnetlib.uoamonitorservice.entities.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryDAO extends MongoRepository<Category, String> {
    List<Category> findAll();
    List<Category> findByDefaultId(String DefaultId);

    List<Category> findBySubCategoriesContaining(String subCategory);

    Optional<Category> findById(String Id);

    void delete(String Id);

    Category save(Category category);
}
