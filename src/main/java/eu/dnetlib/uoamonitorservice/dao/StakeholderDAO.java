package eu.dnetlib.uoamonitorservice.dao;

import eu.dnetlib.uoamonitorservice.entities.Stakeholder;
import eu.dnetlib.uoamonitorservice.primitives.StakeholderType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StakeholderDAO extends MongoRepository<Stakeholder, String> {
    /**
     * All Stakeholders
     */
    List<Stakeholder> findAll();

    List<Stakeholder> findByDefaultId(String id);

    List<Stakeholder> findByType(StakeholderType Type);

    List<Stakeholder> findByTypeAndDefaultId(StakeholderType Type, String id);

    default List<Stakeholder> allStakeholders() {
        return allStakeholders(null);
    }

    default List<Stakeholder> allStakeholders(StakeholderType type) {
        return allStakeholders(type, null);
    }

    default List<Stakeholder> allStakeholders(StakeholderType type, String id) {
        if (type != null && id != null) {
            return findByTypeAndDefaultId(type, id);
        } else if (id != null) {
            return findByDefaultId(id);
        } else if (type != null) {
            return findByType(type);
        } else {
            return findAll();
        }
    }

    /**
     * Default Stakeholders
     */
    List<Stakeholder> findByDefaultIdIsNull();

    List<Stakeholder> findByDefaultIdIsNullAndType(StakeholderType Type);

    default List<Stakeholder> defaultStakeholders(StakeholderType type) {
        if (type == null) {
            return findByDefaultIdIsNull();
        }
        return findByDefaultIdIsNullAndType(type);
    }

    /**
     * Standalone Stakeholders (not umbrella)
     */
    List<Stakeholder> findByDefaultIdIsNotNullAndStandaloneIsTrueAndUmbrellaIsNull();

    List<Stakeholder> findByStandaloneIsTrueAndUmbrellaIsNullAndDefaultId(String id);

    List<Stakeholder> findByDefaultIdIsNotNullAndStandaloneIsTrueAndUmbrellaIsNullAndType(StakeholderType Type);

    List<Stakeholder> findByStandaloneIsTrueAndUmbrellaIsNullAndTypeAndDefaultId(StakeholderType Type, String id);

    default List<Stakeholder> standaloneStakeholders() {
        return standaloneStakeholders(null);
    }

    default List<Stakeholder> standaloneStakeholders(StakeholderType type) {
        return standaloneStakeholders(type, null);
    }

    default List<Stakeholder> standaloneStakeholders(StakeholderType type, String id) {
        if (type != null && id != null) {
            return findByStandaloneIsTrueAndUmbrellaIsNullAndTypeAndDefaultId(type, id);
        } else if (id != null) {
            return findByStandaloneIsTrueAndUmbrellaIsNullAndDefaultId(id);
        } else if (type != null) {
            return findByDefaultIdIsNotNullAndStandaloneIsTrueAndUmbrellaIsNullAndType(type);
        } else {
            return findByDefaultIdIsNotNullAndStandaloneIsTrueAndUmbrellaIsNull();
        }
    }

    /**
     * Dependent Stakeholders
     */
    List<Stakeholder> findByDefaultIdIsNotNullAndStandaloneIsFalse();

    List<Stakeholder> findByStandaloneIsFalseAndDefaultId(String id);

    List<Stakeholder> findByDefaultIdIsNotNullAndStandaloneIsFalseAndType(StakeholderType Type);

    List<Stakeholder> findByStandaloneIsFalseAndTypeAndDefaultId(StakeholderType Type, String id);

    default List<Stakeholder> dependentStakeholders() {
        return dependentStakeholders(null);
    }

    default List<Stakeholder> dependentStakeholders(StakeholderType type) {
        return dependentStakeholders(type, null);
    }

    default List<Stakeholder> dependentStakeholders(StakeholderType type, String id) {
        if (type != null && id != null) {
            return findByStandaloneIsFalseAndTypeAndDefaultId(type, id);
        } else if (id != null) {
            return findByStandaloneIsFalseAndDefaultId(id);
        } else if (type != null) {
            return findByDefaultIdIsNotNullAndStandaloneIsFalseAndType(type);
        } else {
            return findByDefaultIdIsNotNullAndStandaloneIsFalse();
        }
    }

    /**
     * Umbrella Stakeholders
     */
    List<Stakeholder> findByUmbrellaNotNull();

    List<Stakeholder> findByUmbrellaNotNullAndDefaultId(String id);

    List<Stakeholder> findByUmbrellaNotNullAndType(StakeholderType Type);

    List<Stakeholder> findByUmbrellaNotNullAndTypeAndDefaultId(StakeholderType Type, String id);

    default List<Stakeholder> umbrellaStakeholders() {
        return umbrellaStakeholders(null);
    }

    default List<Stakeholder> umbrellaStakeholders(StakeholderType type) {
        return umbrellaStakeholders(type, null);
    }

    default List<Stakeholder> umbrellaStakeholders(StakeholderType type, String id) {
        if (type != null && id != null) {
            return findByUmbrellaNotNullAndTypeAndDefaultId(type, id);
        } else if (id != null) {
            return findByUmbrellaNotNullAndDefaultId(id);
        } else if (type != null) {
            return findByUmbrellaNotNullAndType(type);
        } else {
            return findByUmbrellaNotNull();
        }
    }

    /**
     * Browse Stakeholders
     * */
    List<Stakeholder> findByDefaultIdNotNullAndStandaloneIsTrue();

    List<Stakeholder> findByStandaloneIsTrueAndDefaultId(String id);

    List<Stakeholder> findByDefaultIdNotNullAndStandaloneIsTrueAndType(StakeholderType Type);

    List<Stakeholder> findByStandaloneIsTrueAndTypeAndDefaultId(StakeholderType Type, String id);

    default List<Stakeholder> browseStakeholders() {
        return browseStakeholders(null);
    }

    default List<Stakeholder> browseStakeholders(StakeholderType type) {
        return browseStakeholders(type, null);
    }

    default List<Stakeholder> browseStakeholders(StakeholderType type, String id) {
        if (type != null && id != null) {
            return findByStandaloneIsTrueAndTypeAndDefaultId(type, id);
        } else if (id != null) {
            return findByStandaloneIsTrueAndDefaultId(id);
        } else if (type != null) {
            return findByDefaultIdNotNullAndStandaloneIsTrueAndType(type);
        } else {
            return findByDefaultIdNotNullAndStandaloneIsTrue();
        }
    }

    /**
     * Other method
     */

    List<Stakeholder> findByDefaultIdAndCopyIsTrue(String defaultId);

    List<Stakeholder> findByTopicsContaining(String topic);

    Optional<Stakeholder> findById(String id);

    Optional<Stakeholder> findByAlias(String alias);


    void delete(String Id);

    Stakeholder save(Stakeholder stakeholder);
}
