package eu.dnetlib.uoamonitorservice.dao;

import eu.dnetlib.uoamonitorservice.entities.Topic;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TopicDAO extends MongoRepository<Topic, String> {
    List<Topic> findAll();
    List<Topic> findByDefaultId(String DefaultId);

    List<Topic> findByCategoriesContaining(String category);

    Optional<Topic> findById(String Id);

    void delete(String Id);

    Topic save(Topic topic);
}
