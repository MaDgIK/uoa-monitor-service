package eu.dnetlib.uoamonitorservice.controllers;

import eu.dnetlib.uoamonitorservice.dto.MoveIndicator;
import eu.dnetlib.uoamonitorservice.dto.SectionFull;
import eu.dnetlib.uoamonitorservice.dto.SubCategoryFull;
import eu.dnetlib.uoamonitorservice.entities.*;
import eu.dnetlib.uoamonitorservice.handlers.PathNotValidException;
import eu.dnetlib.uoamonitorservice.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class SectionController {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final StakeholderService stakeholderService;
    private final TopicService topicService;
    private final CategoryService categoryService;
    private final SubCategoryService subCategoryService;
    private final SectionService sectionService;

    @Autowired
    public SectionController(StakeholderService stakeholderService, TopicService topicService, CategoryService categoryService,
                             SubCategoryService subCategoryService, SectionService sectionService) {
        this.stakeholderService = stakeholderService;
        this.topicService = topicService;
        this.categoryService = categoryService;
        this.subCategoryService = subCategoryService;
        this.sectionService = sectionService;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/{subcategoryId}/save/{index}", method = RequestMethod.POST)
    public SectionFull saveSection(@PathVariable("stakeholderId") String stakeholderId,
                                   @PathVariable("topicId") String topicId,
                                   @PathVariable("categoryId") String categoryId,
                                   @PathVariable("subcategoryId") String subcategoryId,
                                   @PathVariable("index") String index,
                                   @RequestBody SectionFull sectionFull) {
        log.debug("save section");
        log.debug("Name: " + sectionFull.getTitle() + " - Id: " + sectionFull.getId() + " - Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId + " - SubCategory: " + subcategoryId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        SubCategory subCategory = this.subCategoryService.findByPath(category, subcategoryId);
        if(sectionFull.getId() != null) {
            this.sectionService.findByPath(subCategory, sectionFull.getId());
        }
        return this.sectionService.save(stakeholder, subCategory, new Section(sectionFull), Integer.parseInt(index));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/{subcategoryId}/{sectionId}/delete", method = RequestMethod.DELETE)
    public boolean deleteSection(@PathVariable("stakeholderId") String stakeholderId,
                                 @PathVariable("topicId") String topicId,
                                 @PathVariable("categoryId") String categoryId,
                                 @PathVariable("subcategoryId") String subcategoryId,
                                 @PathVariable("sectionId") String sectionId,
                                 @RequestParam(required = false) String children) {
        log.debug("delete section");
        log.debug("Id: " + sectionId + " - Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId + " - SubCategory: " + subcategoryId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        SubCategory subCategory = this.subCategoryService.findByPath(category, subcategoryId);
        Section section = this.sectionService.findByPath(subCategory, sectionId);
        this.sectionService.delete(stakeholder.getType(), section, true);
        return true;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/{subcategoryId}/moveIndicator", method = RequestMethod.POST)
    public SubCategoryFull moveIndicator(@PathVariable("stakeholderId") String stakeholderId,
                                         @PathVariable("topicId") String topicId,
                                         @PathVariable("categoryId") String categoryId,
                                         @PathVariable("subcategoryId") String subcategoryId,
                                         @RequestBody MoveIndicator moveIndicator) {
        log.debug("move indicator");
        log.debug("Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId + " - SubCategory: " + subcategoryId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        SubCategory subCategory = this.subCategoryService.findByPath(category, subcategoryId);
        return this.subCategoryService.moveIndicator(stakeholder, subCategory, moveIndicator);
    }
}
