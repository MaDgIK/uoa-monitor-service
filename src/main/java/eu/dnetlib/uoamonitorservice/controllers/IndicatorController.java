package eu.dnetlib.uoamonitorservice.controllers;


import eu.dnetlib.uoamonitorservice.dto.SectionFull;
import eu.dnetlib.uoamonitorservice.dto.StakeholderFull;
import eu.dnetlib.uoamonitorservice.entities.*;
import eu.dnetlib.uoamonitorservice.primitives.Visibility;
import eu.dnetlib.uoamonitorservice.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class IndicatorController {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final StakeholderService stakeholderService;
    private final TopicService topicService;
    private final CategoryService categoryService;
    private final SubCategoryService subCategoryService;
    private final SectionService sectionService;
    private final IndicatorService indicatorService;

    @Autowired
    public IndicatorController(StakeholderService stakeholderService, TopicService topicService, CategoryService categoryService,
                             SubCategoryService subCategoryService, SectionService sectionService, IndicatorService indicatorService) {
        this.stakeholderService = stakeholderService;
        this.topicService = topicService;
        this.categoryService = categoryService;
        this.subCategoryService = subCategoryService;
        this.sectionService = sectionService;
        this.indicatorService = indicatorService;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/{subcategoryId}/save-bulk", method = RequestMethod.POST)
    public StakeholderFull saveBulkIndicators(@PathVariable("stakeholderId") String stakeholderId,
                                              @PathVariable("topicId") String topicId,
                                              @PathVariable("categoryId") String categoryId,
                                              @PathVariable("subcategoryId") String subcategoryId,
                                              @RequestBody List<SectionFull> sections) throws UnsupportedEncodingException {
        log.debug("save bulk indicators");
        log.debug("Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId + " - SubCategory: " + subcategoryId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        SubCategory subCategory = this.subCategoryService.findByPath(category, subcategoryId);
        this.sectionService.saveBulk(stakeholder, subCategory, sections);
        return this.stakeholderService.getFullStakeholder(stakeholder);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/{subcategoryId}/{sectionId}/save", method = RequestMethod.POST)
    public Indicator saveIndicator(@PathVariable("stakeholderId") String stakeholderId,
                                   @PathVariable("topicId") String topicId,
                                   @PathVariable("categoryId") String categoryId,
                                   @PathVariable("subcategoryId") String subcategoryId,
                                   @PathVariable("sectionId") String sectionId,
                                   @RequestBody Indicator indicator) {
        log.debug("save indicator");
        log.debug("Name: " + indicator.getName() + " - Id: " + indicator.getId() + " - Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId + " - SubCategory: " + subcategoryId + " - Section: " + sectionId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        SubCategory subCategory = this.subCategoryService.findByPath(category, subcategoryId);
        Section section = this.sectionService.findByPath(subCategory, sectionId);
        if(indicator.getId() != null) {
            this.indicatorService.findByPath(section, indicator.getId());
        }
        return this.indicatorService.save(stakeholder, section, indicator);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/{subcategoryId}/{sectionId}/{indicatorId}/delete", method = RequestMethod.DELETE)
    public boolean deleteIndicator(@PathVariable("stakeholderId") String stakeholderId,
                                   @PathVariable("topicId") String topicId,
                                   @PathVariable("categoryId") String categoryId,
                                   @PathVariable("subcategoryId") String subcategoryId,
                                   @PathVariable("sectionId") String sectionId,
                                   @PathVariable("indicatorId") String indicatorId,
                                   @RequestParam(required = false) String children) {
        log.debug("delete indicator");
        log.debug("Id: " + indicatorId + " - Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId + " - SubCategory: " + subcategoryId + " - Section: " + sectionId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        SubCategory subCategory = this.subCategoryService.findByPath(category, subcategoryId);
        Section section = this.sectionService.findByPath(subCategory, sectionId);
        Indicator indicator = this.indicatorService.findByPath(section, indicatorId);
        this.indicatorService.delete(stakeholder.getType(), indicator, true);
        return true;
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/{subcategoryId}/{sectionId}/reorder", method = RequestMethod.POST)
    public List<Indicator> reorderIndicators(@PathVariable("stakeholderId") String stakeholderId,
                                             @PathVariable("topicId") String topicId,
                                             @PathVariable("categoryId") String categoryId,
                                             @PathVariable("subcategoryId") String subcategoryId,
                                             @PathVariable("sectionId") String sectionId,
                                             @RequestBody List<String> indicators) {
        log.debug("Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId + " - SubCategory: " + subcategoryId + " - Section: " + sectionId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        SubCategory subCategory = this.subCategoryService.findByPath(category, subcategoryId);
        Section section = this.sectionService.findByPath(subCategory, sectionId);
        return this.sectionService.reorderIndicators(stakeholder, section, indicators).getIndicators();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/{subcategoryId}/{sectionId}/{indicatorId}/change-visibility", method = RequestMethod.POST)
    public Indicator changeIndicatorVisibility(@PathVariable("stakeholderId") String stakeholderId,
                                               @PathVariable("topicId") String topicId,
                                               @PathVariable("categoryId") String categoryId,
                                               @PathVariable("subcategoryId") String subcategoryId,
                                               @PathVariable("sectionId") String sectionId,
                                               @PathVariable("indicatorId") String indicatorId,
                                               @RequestParam("visibility") Visibility visibility) {
        log.debug("change indicator visibility: " + visibility);
        log.debug("Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId + " - SubCategory: " + subcategoryId + " - Section: " + sectionId + " - Indicator: " + indicatorId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        SubCategory subCategory = this.subCategoryService.findByPath(category, subcategoryId);
        Section section = this.sectionService.findByPath(subCategory, sectionId);
        Indicator indicator = this.indicatorService.findByPath(section, indicatorId);
        return this.indicatorService.changeVisibility(stakeholder.getType(), stakeholder.getAlias(), indicator, visibility);
    }
}
