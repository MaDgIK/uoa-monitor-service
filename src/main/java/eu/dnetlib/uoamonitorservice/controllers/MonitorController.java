package eu.dnetlib.uoamonitorservice.controllers;

import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import eu.dnetlib.uoaadmintoolslibrary.entities.fullEntities.*;
import eu.dnetlib.uoaadmintoolslibrary.services.PageService;
import eu.dnetlib.uoaadmintoolslibrary.services.PortalService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

@RestController
@RequestMapping(value={"/monitor", "/funder", "/project", "/ri", "/organization"})
@CrossOrigin(origins = "*")
public class MonitorController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PortalService portalService;

    @Autowired
    private PageService pageService;

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public PortalResponse updatePortal(@RequestBody Portal portal) {
        String old_pid = portalService.getPortalById(portal.getId()).getPid();
        String new_pid = portal.getPid();

        PortalResponse portalResponse = portalService.updatePortal(portal);

        if (!old_pid.equals(new_pid)) {
            pageService.updatePid(old_pid, new_pid, portal.getType());
        }

        return portalResponse;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public PortalResponse insertPortal(@RequestBody Portal portal) {
        PortalResponse portalResponse = portalService.insertPortal(portal);
        return portalResponse;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public Boolean deletePortals(@RequestBody List<String> portals) {
        portals.forEach(id -> portalService.deletePortal(id));
        return true;
    }
}

