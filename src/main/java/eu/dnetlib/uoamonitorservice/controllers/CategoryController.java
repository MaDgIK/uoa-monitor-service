package eu.dnetlib.uoamonitorservice.controllers;

import eu.dnetlib.uoamonitorservice.dto.CategoryFull;
import eu.dnetlib.uoamonitorservice.entities.Category;
import eu.dnetlib.uoamonitorservice.entities.Stakeholder;
import eu.dnetlib.uoamonitorservice.entities.Topic;
import eu.dnetlib.uoamonitorservice.primitives.Visibility;
import eu.dnetlib.uoamonitorservice.service.CategoryService;
import eu.dnetlib.uoamonitorservice.service.StakeholderService;
import eu.dnetlib.uoamonitorservice.service.TopicService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class CategoryController {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final StakeholderService stakeholderService;
    private final TopicService topicService;
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(StakeholderService stakeholderService, TopicService topicService, CategoryService categoryService) {
        this.stakeholderService = stakeholderService;
        this.topicService = topicService;
        this.categoryService = categoryService;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/save", method = RequestMethod.POST)
    public CategoryFull saveCategory(@PathVariable("stakeholderId") String stakeholderId,
                                              @PathVariable("topicId") String topicId,
                                              @RequestBody CategoryFull categoryFull) {
        log.debug("save category");
        log.debug("Alias: " + categoryFull.getAlias() + " - Id: " + categoryFull.getId() + " - Stakeholder: " + stakeholderId + " - Topic: " + topicId);
        Stakeholder stakeholder = stakeholderService.findByPath(stakeholderId);
        Topic topic = topicService.findByPath(stakeholder, topicId);
        if(categoryFull.getId() != null) {
            this.categoryService.findByPath(topic, categoryFull.getId());
        }
        return this.categoryService.save(stakeholder, topic, new Category(categoryFull));
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/delete", method = RequestMethod.DELETE)
    public boolean deleteCategory(@PathVariable("stakeholderId") String stakeholderId,
                                  @PathVariable("topicId") String topicId,
                                  @PathVariable("categoryId") String categoryId,
                                  @RequestParam(required = false) String children) {
        log.debug("delete category");
        log.debug("Id: " + categoryId + " - Stakeholder: " + stakeholderId + " - Topic: " + topicId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        this.categoryService.delete(stakeholder.getType(), category, true);
        return true;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/reorder", method = RequestMethod.POST)
    public List<CategoryFull> reorderCategories(@PathVariable("stakeholderId") String stakeholderId,
                                            @PathVariable("topicId") String topicId,
                                            @RequestBody List<String> categories) {
        log.debug("reorder categories");
        log.debug("Stakeholder: " + stakeholderId + " - Topic: " + topicId);
        Stakeholder stakeholder = stakeholderService.findByPath(stakeholderId);
        Topic topic = topicService.findByPath(stakeholder, topicId);
        return this.topicService.reorderCategories(stakeholder, topic, categories).getCategories();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/change-visibility", method = RequestMethod.POST)
    public CategoryFull changeCategoryVisibility(@PathVariable("stakeholderId") String stakeholderId,
                                                 @PathVariable("topicId") String topicId,
                                                 @PathVariable("categoryId") String categoryId,
                                                 @RequestParam("visibility") Visibility visibility, @RequestParam(defaultValue = "false") Boolean propagate) {
        log.debug("change category visibility: " + visibility + " - toggle propagate: " + propagate);
        log.debug("Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        return  this.categoryService.changeVisibility(stakeholder.getType(), stakeholder.getAlias(), category, visibility, propagate);
    }
}
