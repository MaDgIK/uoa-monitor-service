package eu.dnetlib.uoamonitorservice.controllers;

import eu.dnetlib.uoaadmintoolslibrary.entities.Portal;
import eu.dnetlib.uoaadmintoolslibrary.services.PortalService;
import eu.dnetlib.uoamonitorservice.dto.BuildStakeholder;
import eu.dnetlib.uoamonitorservice.dto.ManageStakeholders;
import eu.dnetlib.uoamonitorservice.dto.StakeholderFull;
import eu.dnetlib.uoamonitorservice.dto.UpdateUmbrella;
import eu.dnetlib.uoamonitorservice.entities.Stakeholder;
import eu.dnetlib.uoamonitorservice.primitives.Umbrella;
import eu.dnetlib.uoamonitorservice.primitives.Visibility;
import eu.dnetlib.uoamonitorservice.service.CommonService;
import eu.dnetlib.uoamonitorservice.service.StakeholderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class StakeholderController {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final PortalService portalService;
    private final StakeholderService stakeholderService;
    private final CommonService commonService;

    @Autowired
    public StakeholderController(PortalService portalService, StakeholderService stakeholderService, CommonService commonService) {
        this.portalService = portalService;
        this.stakeholderService = stakeholderService;
        this.commonService = commonService;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/stakeholder/alias", method = RequestMethod.GET)
    public List<String> getAllReservedStakeholderAlias(@RequestParam(required = false) String type) {
        List<String> stakeholderAlias = this.stakeholderService.getAllAliases(type);
        if(type == null ) {
            stakeholderAlias.add("all");
            stakeholderAlias.add("default");
            stakeholderAlias.add("alias");
        }
        return stakeholderAlias;
    }

    @PreAuthorize("hasAnyAuthority(" +
            "@AuthorizationService.PORTAL_ADMIN, " +
            "@AuthorizationService.curator(#buildStakeholder.stakeholder.getType()))")
    @RequestMapping(value = "/build-stakeholder", method = RequestMethod.POST)
    public Stakeholder buildStakeholder(@RequestBody BuildStakeholder buildStakeholder) {
        Stakeholder stakeholder = buildStakeholder.getStakeholder();
        log.debug("build stakeholder");
        log.debug("Alias: " + stakeholder.getAlias());
        Portal portal = portalService.getPortal(stakeholder.getAlias());
        if (portal == null) {
            portal = new Portal();
            portal.setPid(stakeholder.getAlias());
            portal.setName(stakeholder.getName());
            portal.setType(stakeholder.getType());
            portalService.insertPortal(portal);
        }
        stakeholder.setStandalone(buildStakeholder.isStandalone());
        if (buildStakeholder.isUmbrella()) {
            stakeholder.setUmbrella(new Umbrella<>());
        }
        return this.stakeholderService.buildStakeholder(stakeholder, buildStakeholder.getCopyId());
    }

    @PreAuthorize("hasAnyAuthority(" + "@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/stakeholder/all", method = RequestMethod.GET)
    public List<Stakeholder> getAllStakeholders(@RequestParam(required = false) String type) {
        return this.stakeholderService.getAll(type);
    }

    @RequestMapping(value = "/stakeholder", method = RequestMethod.GET)
    public List<Stakeholder> getVisibleStakeholders(@RequestParam(required = false) String type,
                                                    @RequestParam(required = false) String defaultId) {
        return stakeholderService.getVisibleStakeholders(type, defaultId);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/my-stakeholder", method = RequestMethod.GET)
    public ManageStakeholders getManagedStakeholders(@RequestParam(required = false) String type) {
        return stakeholderService.getManageStakeholders(type);
    }

    @RequestMapping(value = "/stakeholder/{alias:.+}", method = RequestMethod.GET)
    public StakeholderFull getStakeholder(@PathVariable("alias") String alias) {
        StakeholderFull stakeholder = this.stakeholderService.getFullStakeholder(this.stakeholderService.findByAlias(alias));
        if (stakeholder == null) {
            this.commonService.unauthorized("Get stakeholder: You are not authorized to access stakeholder with alias: " + alias);
        }
        return stakeholder;
    }

    @RequestMapping(value = "/stakeholder/{parent:.+}/{type}/{child:.+}", method = RequestMethod.GET)
    public StakeholderFull getStakeholderWithParent(@PathVariable("parent") String parent, @PathVariable("type") String type, @PathVariable("child") String child) {
        StakeholderFull stakeholder =  this.stakeholderService.getFullStakeholderWithParents(this.stakeholderService.findByAlias(child), type, parent);
        if (stakeholder == null) {
            this.commonService.unauthorized("Get stakeholder: You are not authorized to access stakeholder with alias: " + child);
        }
        return stakeholder;

    }

    @PreAuthorize("hasAnyAuthority("
            + "@AuthorizationService.PORTAL_ADMIN, "
            + "@AuthorizationService.curator(#stakeholder.getType()), "
            + "@AuthorizationService.manager(#stakeholder.getType(), #stakeholder.getAlias()) "
            + ")")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Stakeholder saveStakeholder(@RequestBody Stakeholder stakeholder) {
        log.debug("save stakeholder");
        log.debug("Alias: " + stakeholder.getAlias() + " - Id: " + stakeholder.getId());
        return this.stakeholderService.save(stakeholder);
    }

    @PreAuthorize("hasAnyAuthority("
            + "@AuthorizationService.PORTAL_ADMIN, "
            + "@AuthorizationService.curator(#stakeholder.getType()), "
            + "@AuthorizationService.manager(#stakeholder.getType(), #stakeholder.getAlias()) "
            + ")")
    @RequestMapping(value = "/save/full", method = RequestMethod.POST)
    public StakeholderFull saveStakeholderFull(@RequestBody StakeholderFull stakeholder) {
        log.debug("save stakeholder");
        log.debug("Alias: " + stakeholder.getAlias() + " - Id: " + stakeholder.getId());
        return this.stakeholderService.getFullStakeholder(this.stakeholderService.save(new Stakeholder(stakeholder, this.stakeholderService)));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/delete", method = RequestMethod.DELETE)
    public boolean deleteStakeholder(@PathVariable("stakeholderId") String id) {
        log.debug("delete stakeholder");
        log.debug("Id: " + id);
        Portal portal = portalService.getPortal(this.stakeholderService.delete(id));
        if (portal != null) {
            portalService.deletePortal(portal.getId());
        }
        return true;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/change-visibility", method = RequestMethod.POST)
    public StakeholderFull changeStakeholderVisibility(@PathVariable("stakeholderId") String stakeholderId,
                                                       @RequestParam("visibility") Visibility visibility, @RequestParam(defaultValue = "false") Boolean propagate) {
        log.debug("change stakeholder visibility: " + visibility + " - toggle propagate: " + propagate);
        log.debug("Stakeholder: " + stakeholderId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        return this.stakeholderService.changeVisibility(stakeholder, visibility, propagate);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/umbrella", method = RequestMethod.POST)
    public Umbrella<Stakeholder> updateUmbrella(@PathVariable("stakeholderId") String stakeholderId, @RequestBody UpdateUmbrella update) {
        log.debug("update stakeholder umbrella");
        log.debug("Stakeholder: " + stakeholderId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        return this.stakeholderService.updateUmbrella(stakeholder, update);
    }
}
