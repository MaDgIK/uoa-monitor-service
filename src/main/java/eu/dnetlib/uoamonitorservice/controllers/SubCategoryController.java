package eu.dnetlib.uoamonitorservice.controllers;

import eu.dnetlib.uoamonitorservice.dto.SubCategoryFull;
import eu.dnetlib.uoamonitorservice.entities.Category;
import eu.dnetlib.uoamonitorservice.entities.Stakeholder;
import eu.dnetlib.uoamonitorservice.entities.SubCategory;
import eu.dnetlib.uoamonitorservice.entities.Topic;
import eu.dnetlib.uoamonitorservice.primitives.Visibility;
import eu.dnetlib.uoamonitorservice.service.CategoryService;
import eu.dnetlib.uoamonitorservice.service.StakeholderService;
import eu.dnetlib.uoamonitorservice.service.SubCategoryService;
import eu.dnetlib.uoamonitorservice.service.TopicService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class SubCategoryController {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final StakeholderService stakeholderService;
    private final TopicService topicService;
    private final CategoryService categoryService;
    private final SubCategoryService subCategoryService;

    @Autowired
    public SubCategoryController(StakeholderService stakeholderService, TopicService topicService, CategoryService categoryService, SubCategoryService subCategoryService) {
        this.stakeholderService = stakeholderService;
        this.topicService = topicService;
        this.categoryService = categoryService;
        this.subCategoryService = subCategoryService;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/save", method = RequestMethod.POST)
    public SubCategoryFull saveSubCategory(@PathVariable("stakeholderId") String stakeholderId,
                                           @PathVariable("topicId") String topicId,
                                           @PathVariable("categoryId") String categoryId,
                                           @RequestBody SubCategoryFull subcategoryFull) {
        log.debug("save subcategory");
        log.debug("Alias: " + subcategoryFull.getAlias() + " - Id: " + subcategoryFull.getId() + " - Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        if(subcategoryFull.getId() != null) {
            this.subCategoryService.findByPath(category, subcategoryFull.getId());
        }
        return this.subCategoryService.save(stakeholder, category, new SubCategory(subcategoryFull));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/{subcategoryId}/delete", method = RequestMethod.DELETE)
    public boolean deleteSubCategory(@PathVariable("stakeholderId") String stakeholderId,
                                     @PathVariable("topicId") String topicId,
                                     @PathVariable("categoryId") String categoryId,
                                     @PathVariable("subcategoryId") String subcategoryId,
                                     @RequestParam(required = false) String children) {
        log.debug("delete subcategory");
        log.debug("Id: " + subcategoryId + " - Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        SubCategory subCategory = this.subCategoryService.findByPath(category, subcategoryId);
        this.subCategoryService.delete(stakeholder.getType(), subCategory, true);
        return true;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/reorder", method = RequestMethod.POST)
    public List<SubCategoryFull> reorderSubCategories(@PathVariable("stakeholderId") String stakeholderId,
                                                      @PathVariable("topicId") String topicId,
                                                      @PathVariable("categoryId") String categoryId,
                                                      @RequestBody List<String> subCategories) {
        log.debug("reorder subCategories");
        log.debug("Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        return this.categoryService.reorderSubCategories(stakeholder, category, subCategories).getSubCategories();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/{categoryId}/{subcategoryId}/change-visibility", method = RequestMethod.POST)
    public SubCategoryFull changeSubCategoryVisibility(@PathVariable("stakeholderId") String stakeholderId,
                                                       @PathVariable("topicId") String topicId,
                                                       @PathVariable("categoryId") String categoryId,
                                                       @PathVariable("subcategoryId") String subcategoryId,
                                                       @RequestParam("visibility") Visibility visibility,
                                                       @RequestParam(defaultValue = "false") Boolean propagate) {
        log.debug("change subCategory visibility: " + visibility + " - toggle propagate: " + propagate);
        log.debug("Stakeholder: " + stakeholderId + " - Topic: " + topicId + " - Category: " + categoryId + " - SubCategory: " + subcategoryId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        Category category = this.categoryService.findByPath(topic, categoryId);
        SubCategory subCategory = this.subCategoryService.findByPath(category, subcategoryId);
        return this.subCategoryService.changeVisibility(stakeholder.getType(), stakeholder.getAlias(), subCategory, visibility, propagate);
    }
}
