package eu.dnetlib.uoamonitorservice.controllers;

import eu.dnetlib.uoamonitorservice.dto.TopicFull;
import eu.dnetlib.uoamonitorservice.entities.Stakeholder;
import eu.dnetlib.uoamonitorservice.entities.Topic;
import eu.dnetlib.uoamonitorservice.primitives.Visibility;
import eu.dnetlib.uoamonitorservice.service.StakeholderService;
import eu.dnetlib.uoamonitorservice.service.TopicService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class TopicController {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final TopicService topicService;
    private final StakeholderService stakeholderService;

    @Autowired
    public TopicController(TopicService topicService, StakeholderService stakeholderService) {
        this.topicService = topicService;
        this.stakeholderService = stakeholderService;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/save", method = RequestMethod.POST)
    public TopicFull saveTopic(@PathVariable("stakeholderId") String stakeholderId, @RequestBody TopicFull topicFull) {
        log.debug("save topic");
        log.debug("Alias: " + topicFull.getAlias() + " - Id: " + topicFull.getId() + " - Stakeholder: " + stakeholderId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        if(topicFull.getId() != null) {
            this.topicService.findByPath(stakeholder, topicFull.getId());
        }
        return this.topicService.save(stakeholder, new Topic(topicFull));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/delete", method = RequestMethod.DELETE)
    public boolean deleteTopic(@PathVariable("stakeholderId") String stakeholderId,
                               @PathVariable("topicId") String topicId,
                               @RequestParam(required = false) String children) {
        log.debug("delete topic");
        log.debug("Id: " + topicId + " - Stakeholder: " + stakeholderId);
        Stakeholder stakeholder = stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        this.topicService.delete(stakeholder.getType(), topic, true);
        return true;
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/reorder", method = RequestMethod.POST)
    public List<TopicFull> reorderTopics(@PathVariable("stakeholderId") String stakeholderId,
                                         @RequestBody List<String> topics) {
        log.debug("reorder topics");
        log.debug("Stakeholder: " + stakeholderId);
        Stakeholder stakeholder = stakeholderService.findByPath(stakeholderId);
        return this.stakeholderService.reorderTopics(stakeholder, topics).getTopics();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{stakeholderId}/{topicId}/change-visibility", method = RequestMethod.POST)
    public TopicFull changeTopicVisibility(@PathVariable("stakeholderId") String stakeholderId,
                                           @PathVariable("topicId") String topicId,
                                           @RequestParam("visibility") Visibility visibility, @RequestParam(defaultValue = "false") Boolean propagate) {
        log.debug("change topic visibility: " + visibility + " - toggle propagate: " + propagate);
        log.debug("Stakeholder: " + stakeholderId + " - Topic: " + topicId);
        Stakeholder stakeholder = this.stakeholderService.findByPath(stakeholderId);
        Topic topic = this.topicService.findByPath(stakeholder, topicId);
        return this.topicService.changeVisibility(stakeholder.getType(), stakeholder.getAlias(), topic, visibility, propagate);
    }
}
