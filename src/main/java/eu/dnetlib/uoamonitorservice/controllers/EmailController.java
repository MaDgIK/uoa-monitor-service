package eu.dnetlib.uoamonitorservice.controllers;

import eu.dnetlib.uoaadmintoolslibrary.emailSender.EmailSender;
import eu.dnetlib.uoaadmintoolslibrary.entities.email.Email;
import eu.dnetlib.uoaadmintoolslibrary.entities.email.EmailRecaptcha;
import eu.dnetlib.uoaadmintoolslibrary.handlers.InvalidReCaptchaException;
import eu.dnetlib.uoaadmintoolslibrary.recaptcha.VerifyRecaptcha;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
public class EmailController {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final EmailSender emailSender;
    private final VerifyRecaptcha verifyRecaptcha;

    @Autowired
    public EmailController(EmailSender emailSender, VerifyRecaptcha verifyRecaptcha) {
        this.emailSender = emailSender;
        this.verifyRecaptcha = verifyRecaptcha;
    }

    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    public Boolean contact(@RequestBody EmailRecaptcha form) throws InvalidReCaptchaException {
        verifyRecaptcha.processResponse(form.getRecaptcha());
        Email email = form.getEmail();
        return emailSender.send(email.getRecipients(), email.getSubject(), email.getBody(), false);
    }

    @RequestMapping(value = "/sendMail", method = RequestMethod.POST)
    public Map<String, ArrayList<String>> sendEmail(@RequestBody Email email,
                                                    @RequestParam(required = false) Optional<Boolean> optional) {
        String successString = "success";
        String failureString = "failure";
        Map<String, ArrayList<String>> mailResults = new HashMap<>();
        boolean bcc = optional.orElse(true);
        for (String userMail : email.getRecipients()) {
            ArrayList<String> sendTo = new ArrayList<>();
            sendTo.add(userMail);
            boolean success = emailSender.send(sendTo, email.getSubject(), email.getBody(), bcc);
            if (success) {
                if (!mailResults.containsKey(successString)) {
                    mailResults.put(successString, new ArrayList<>());
                }
                mailResults.get(successString).add(userMail);
            } else {
                if (!mailResults.containsKey(failureString)) {
                    mailResults.put(failureString, new ArrayList<>());
                }
                mailResults.get(failureString).add(userMail);
            }
        }
        return mailResults;

    }

}