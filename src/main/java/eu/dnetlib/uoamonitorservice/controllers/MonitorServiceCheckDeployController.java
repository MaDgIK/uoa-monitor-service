package eu.dnetlib.uoamonitorservice.controllers;

import eu.dnetlib.uoamonitorservice.service.MonitorDeployService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class MonitorServiceCheckDeployController {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final MonitorDeployService service;

    public MonitorServiceCheckDeployController(MonitorDeployService service) {
        this.service = service;
    }

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from uoa-monitor-service!");
        return "Hello from uoa-monitor-service!";
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
       return this.service.checkEverything();
    }
}
