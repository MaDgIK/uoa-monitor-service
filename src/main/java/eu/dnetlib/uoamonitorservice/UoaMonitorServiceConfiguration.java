package eu.dnetlib.uoamonitorservice;

import eu.dnetlib.uoaadmintoolslibrary.UoaAdminToolsLibraryConfiguration;
import eu.dnetlib.uoamonitorservice.configuration.GlobalVars;
import eu.dnetlib.uoamonitorservice.configuration.properties.APIProperties;
import eu.dnetlib.uoamonitorservice.configuration.properties.MongoConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableConfigurationProperties({ MongoConfig.class, GlobalVars.class, APIProperties.class})
@ComponentScan(basePackages = { "eu.dnetlib.uoamonitorservice" })
@Import({UoaAdminToolsLibraryConfiguration.class})
public class UoaMonitorServiceConfiguration extends WebMvcConfigurerAdapter {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Bean
    public static PropertySourcesPlaceholderConfigurer  propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
