package eu.dnetlib.uoamonitorservice.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PathNotValidException extends RuntimeException {
    public PathNotValidException(String message){
        super(message);
    }
}
