package eu.dnetlib.uoamonitorservice.handlers;

import eu.dnetlib.uoaadmintoolslibrary.responses.ExceptionResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class ExceptionsHandler {
    private final Logger log = LogManager.getLogger(this.getClass());

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ExceptionResponse> entityNotFoundException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Not found Exception");
        response.setErrorMessage("Entity not found Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.NOT_FOUND);
        log.error("entityNotFoundException exception : "+ ex.getMessage());
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ExceptionResponse> conflictException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Not found Exception");
        response.setErrorMessage("Conflict Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.BAD_REQUEST);
        log.error("conflictException exception : "+ ex.getMessage());
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PathNotValidException.class)
    public ResponseEntity<ExceptionResponse> pathNotValidException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Not found Exception");
        response.setErrorMessage("Path not valid Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.NOT_FOUND);
        log.error("pathNotValidException exception : "+ ex.getMessage());
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_FOUND);
    }

}
