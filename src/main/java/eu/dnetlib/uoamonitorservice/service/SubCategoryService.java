package eu.dnetlib.uoamonitorservice.service;

import eu.dnetlib.uoamonitorservice.dao.CategoryDAO;
import eu.dnetlib.uoamonitorservice.dao.StakeholderDAO;
import eu.dnetlib.uoamonitorservice.dao.SubCategoryDAO;
import eu.dnetlib.uoamonitorservice.dao.TopicDAO;
import eu.dnetlib.uoamonitorservice.dto.MoveIndicator;
import eu.dnetlib.uoamonitorservice.dto.SectionFull;
import eu.dnetlib.uoamonitorservice.dto.SectionInfo;
import eu.dnetlib.uoamonitorservice.dto.SubCategoryFull;
import eu.dnetlib.uoamonitorservice.entities.*;
import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.handlers.EntityNotFoundException;
import eu.dnetlib.uoamonitorservice.handlers.PathNotValidException;
import eu.dnetlib.uoamonitorservice.primitives.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubCategoryService {

    private final StakeholderDAO stakeholderDAO;
    private final TopicDAO topicDAO;
    private final CategoryDAO categoryDAO;
    private final SubCategoryDAO dao;

    private final CommonService commonService;
    private final SectionService sectionService;

    @Autowired
    public SubCategoryService(StakeholderDAO stakeholderDAO, TopicDAO topicDAO, CategoryDAO categoryDAO, SubCategoryDAO dao,CommonService commonService, SectionService sectionService) {
        this.stakeholderDAO = stakeholderDAO;
        this.topicDAO = topicDAO;
        this.categoryDAO = categoryDAO;
        this.dao = dao;
        this.commonService = commonService;
        this.sectionService = sectionService;
    }

    public SubCategory find(String id) {
        return dao.findById(id).orElseThrow(() -> new EntityNotFoundException("SubCategory with id: " + id + " not found"));
    }

    public SubCategory findByPath(Category category, String subcategoryId) {
        if (!category.getSubCategories().contains(subcategoryId)) {
            throw new PathNotValidException("SubCategory with id: " + subcategoryId + " not found in Category: " + category.getId());
        }
        return this.dao.findById(subcategoryId).orElseThrow(() -> new EntityNotFoundException("SubCategory with id: " + subcategoryId + " not found"));
    }

    public SubCategoryFull getFullSubCategory(String type, String alias, SubCategory subCategory) {
        if(commonService.hasVisibilityAuthority(type, alias, subCategory)) {
            return  new SubCategoryFull(subCategory, subCategory.getNumbers().stream()
                    .map(sectionId -> this.sectionService.getFullSection(type, alias, sectionId))
                    .collect(Collectors.toList()),
                    subCategory.getCharts().stream()
                            .map(sectionId -> this.sectionService.getFullSection(type, alias, sectionId))
                            .collect(Collectors.toList()));
        } else {
            return null;
        }
    }

    public SubCategoryFull getFullSubCategory(String type, String alias, String subCategoryId) {
        SubCategory subCategory = this.find(subCategoryId);
        return this.getFullSubCategory(type, alias, subCategory);
    }

    public String build(String id) {
        SubCategory subCategory = this.find(id);
        SubCategory copy = subCategory.copy();
        copy.setNumbers(subCategory.getNumbers().stream().map(this.sectionService::build).collect(Collectors.toList()));
        copy.setCharts(subCategory.getCharts().stream().map(this.sectionService::build).collect(Collectors.toList()));
        return this.save(copy).getId();
    }

    public String copy(String id) {
        SubCategory subCategory = this.find(id);
        SubCategory copy = new SubCategory(subCategory);
        copy.setId(null);
        copy.setNumbers(subCategory.getNumbers().stream().map(this.sectionService::copy).collect(Collectors.toList()));
        copy.setCharts(subCategory.getCharts().stream().map(this.sectionService::copy).collect(Collectors.toList()));
        return this.save(copy).getId();
    }

    public SubCategory save(SubCategory subCategory) {
        if(subCategory.getId() != null) {
            SubCategory old = this.find(subCategory.getId());
            subCategory.setNumbers(old.getNumbers());
            subCategory.setCharts(old.getCharts());
        } else {
            subCategory.setCreationDate(new Date());
        }
        subCategory.setUpdateDate(new Date());
        subCategory.getNumbers().forEach(this.sectionService::find);
        subCategory.getCharts().forEach(this.sectionService::find);
        return this.dao.save(subCategory);
    }

    public SubCategoryFull save(Stakeholder stakeholder, Category category, SubCategory subCategory) {
        if (subCategory.getId() != null) {
            if (this.commonService.hasEditAuthority(stakeholder.getType(), stakeholder.getAlias())) {
                SubCategory old = this.find(subCategory.getId());
                subCategory.setNumbers(old.getNumbers());
                subCategory.setCharts(old.getCharts());
                this.updateChildren(subCategory);
                subCategory = this.save(subCategory);
            } else {
                this.commonService.unauthorized("You are not authorized to update stakeholder with id: " + stakeholder.getId());
            }
        } else {
            if (this.commonService.hasCreateAuthority(stakeholder.getType())) {
                subCategory = this.save(subCategory);
                this.createChildren(category, subCategory);
                this.addSubCategory(category, subCategory.getId());
            } else {
                this.commonService.unauthorized("You are not authorized to create a subCategory in stakeholder with id: " + stakeholder.getId());
            }
        }
        return this.getFullSubCategory(stakeholder.getType(), stakeholder.getAlias(), subCategory);
    }

    public void createChildren(Category defaultCategory, SubCategory subCategory) {
        this.categoryDAO.findByDefaultId(defaultCategory.getId()).forEach(category -> {
            this.topicDAO.findByCategoriesContaining(category.getId()).forEach(topic -> {
                this.stakeholderDAO.findByTopicsContaining(topic.getId()).forEach(stakeholder -> {
                    this.save(stakeholder, category, subCategory.copy());
                });
            });
        });
    }

    public void updateChildren(SubCategory subCategory) {
        this.dao.findByDefaultId(subCategory.getId()).forEach(child -> {
            this.save(subCategory.override(child, this.find(subCategory.getId())));
        });
    }

    public SubCategoryFull moveIndicator(Stakeholder stakeholder, SubCategory subCategory, MoveIndicator moveIndicator) {
        if (this.commonService.hasEditAuthority(stakeholder.getType(), stakeholder.getAlias())) {
            Section from = this.sectionService.findByPath(subCategory, moveIndicator.getFrom().getId());
            Section to = this.sectionService.findByPath(subCategory, moveIndicator.getTo().getId());
            from.removeIndicator(moveIndicator.getTarget());
            to.addIndicator(moveIndicator.getTarget());
            this.sectionService.reorderIndicators(stakeholder, from, moveIndicator.getFrom().getIndicators(), false);
            this.sectionService.reorderIndicators(stakeholder, to, moveIndicator.getTo().getIndicators(), false);
            this.moveIndicatorChildren(stakeholder, subCategory, moveIndicator);
            return this.getFullSubCategory(stakeholder.getType(), stakeholder.getAlias(), subCategory);
        } else {
            this.commonService.unauthorized("You are not authorized to move indicators in subCategory with id: " + subCategory.getId());
        }
        return null;
    }

    public void moveIndicatorChildren(Stakeholder defaultStakeholder, SubCategory defaultSubCategory, MoveIndicator moveIndicator) {
        this.stakeholderDAO.findByDefaultIdAndCopyIsTrue(defaultStakeholder.getId()).forEach(stakeholder -> {
            this.dao.findByDefaultId(defaultSubCategory.getId()).stream()
                    .map(subCategory -> this.getFullSubCategory(stakeholder.getType(), stakeholder. getAlias(), subCategory))
                    .collect(Collectors.toList()).forEach(subCategory -> {
                        SectionFull from = subCategory.getSectionByDefaultId(moveIndicator.getFrom().getId()).orElse(null);
                        SectionFull to = subCategory.getSectionByDefaultId(moveIndicator.getTo().getId()).orElse(null);
                        if(from != null && to != null) {
                            Indicator target = from.getIndicatorByDefaultId(moveIndicator.getTarget()).orElse(null);
                            if(target != null) {
                                from.removeIndicator(target.getId());
                                to.addIndicator(target);
                                MoveIndicator moveIndicatorChild = new MoveIndicator(target.getId(),
                                        new SectionInfo(from.getId(), this.commonService.reorder(moveIndicator.getFrom().getIndicators(), from.getIndicators().stream().map(section -> (Common) section).collect(Collectors.toList()))),
                                        new SectionInfo(to.getId(), this.commonService.reorder(moveIndicator.getTo().getIndicators(), to.getIndicators().stream().map(section -> (Common) section).collect(Collectors.toList()))));
                                this.moveIndicator(stakeholder, new SubCategory(subCategory), moveIndicatorChild);
                            }
                        }
                    });
        });
    }

    public SubCategoryFull reorderNumbers(Stakeholder stakeholder, SubCategory subCategory, List<String> numbers) {
        if(this.commonService.hasEditAuthority(stakeholder.getType(), stakeholder.getAlias())) {
            numbers.forEach(this.sectionService::find);
            if (subCategory.getNumbers().size() == numbers.size() && new HashSet<>(subCategory.getNumbers()).containsAll(numbers)) {
                subCategory.setNumbers(numbers);
                subCategory.setUpdateDate(new Date());
                this.reorderChildrenNumbers(stakeholder, subCategory, numbers);
                return this.getFullSubCategory(stakeholder.getType(), stakeholder.getAlias(), this.dao.save(subCategory));
            } else {
                throw new EntityNotFoundException("Some sections dont exist in the subCategory with id " + subCategory.getId());
            }
        } else {
            this.commonService.unauthorized("You are not authorized to reorder sections in subCategory with id: " + subCategory.getId());
        }
        return null;
    }

    public SubCategoryFull reorderCharts(Stakeholder stakeholder, SubCategory subCategory, List<String> charts) {
        if(this.commonService.hasEditAuthority(stakeholder.getType(), stakeholder.getAlias())) {
            charts.forEach(this.sectionService::find);
            if (subCategory.getCharts().size() == charts.size() && new HashSet<>(subCategory.getCharts()).containsAll(charts)) {
                subCategory.setCharts(charts);
                this.reorderChildrenCharts(stakeholder, subCategory, charts);
                return this.getFullSubCategory(stakeholder.getType(), stakeholder.getAlias(), this.dao.save(subCategory));
            } else {
                throw new EntityNotFoundException("Some sections dont exist in the subCategory with id " + subCategory.getId());
            }
        } else {
            this.commonService.unauthorized("You are not authorized to reorder sections in subCategory with id: " + subCategory.getId());
        }
        return null;
    }

    public void reorderChildrenNumbers(Stakeholder defaultStakeholder, SubCategory defaultSubCategory, List<String> defaultSections) {
        this.stakeholderDAO.findByDefaultIdAndCopyIsTrue(defaultStakeholder.getId()).forEach(stakeholder -> {
            this.dao.findByDefaultId(defaultSubCategory.getId()).stream().map(subCategory -> this.getFullSubCategory(stakeholder.getType(), stakeholder.getAlias(), subCategory)).forEach(subCategory -> {
                this.reorderNumbers(stakeholder, new SubCategory(subCategory),
                        this.commonService.reorder(defaultSections, subCategory.getNumbers().stream().map(section -> (Common) section).collect(Collectors.toList())));
            });
        });
    }

    public void reorderChildrenCharts(Stakeholder defaultStakeholder, SubCategory defaultSubCategory, List<String> defaultSections) {
        this.stakeholderDAO.findByDefaultIdAndCopyIsTrue(defaultStakeholder.getId()).forEach(stakeholder -> {
            this.dao.findByDefaultId(defaultSubCategory.getId()).stream().map(subCategory -> this.getFullSubCategory(stakeholder.getType(), stakeholder.getAlias(), subCategory)).forEach(subCategory -> {
                this.reorderCharts(stakeholder, new SubCategory(subCategory),
                        this.commonService.reorder(defaultSections, subCategory.getCharts().stream().map(section -> (Common) section).collect(Collectors.toList())));
            });
        });
    }

    public void delete(String type, SubCategory subCategory, boolean remove) {
        if(this.commonService.hasDeleteAuthority(type)) {
            this.dao.findByDefaultId(subCategory.getId()).forEach(child -> {
                this.delete(type, child.getId(), remove);
            });

            subCategory.getNumbers().forEach(sectionId -> {
                this.sectionService.delete(type, sectionId, false);
            });
            subCategory.getCharts().forEach(sectionId -> {
                this.sectionService.delete(type, sectionId, false);
            });
            if (remove) {
                this.removeSubCategory(subCategory.getId());
            }
            this.dao.delete(subCategory);
        } else {
            this.commonService.unauthorized("Delete subCategory: You are not authorized to delete subCategory with id: " + subCategory.getId());
        }
    }

    public void delete(String type, String id, boolean remove) {
        SubCategory subCategory = this.find(id);
        this.delete(type, subCategory, remove);
    }

    public void addSubCategory(Category category, String id) {
        category.addSubCategory(id);
        category.setUpdateDate(new Date());
        this.categoryDAO.save(category);
    }

    public void removeSubCategory(String id) {
        this.categoryDAO.findBySubCategoriesContaining(id).forEach(category -> {
            category.removeSubCategory(id);
            category.setUpdateDate(new Date());
            this.categoryDAO.save(category);
        });
    }

    public SubCategoryFull changeVisibility(String type, String alias, SubCategoryFull subCategory, Visibility visibility, Boolean propagate) {
        if(this.commonService.hasEditAuthority(type, alias)) {
            subCategory.setVisibility(visibility);
            if(propagate) {
                subCategory.setNumbers(subCategory.getNumbers().stream()
                        .map(section -> this.sectionService.changeVisibility(type, alias, section, visibility))
                        .collect(Collectors.toList()));
                subCategory.setCharts(subCategory.getCharts().stream()
                        .map(section -> this.sectionService.changeVisibility(type, alias, section, visibility))
                        .collect(Collectors.toList()));
            }
            subCategory.update(this.save(new SubCategory(subCategory)));
            return subCategory;
        } else {
            this.commonService.unauthorized("Change subCategory visibility: You are not authorized to update subCategory with id: " + subCategory.getId());
        }
        return null;
    }

    public SubCategoryFull changeVisibility(String type, String alias, SubCategory subCategory, Visibility visibility, Boolean propagate) {
        SubCategoryFull subCategoryFull = this.getFullSubCategory(type, alias, subCategory);
        return this.changeVisibility(type, alias, subCategoryFull, visibility, propagate);
    }
}
