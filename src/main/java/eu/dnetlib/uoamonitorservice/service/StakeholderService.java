package eu.dnetlib.uoamonitorservice.service;

import eu.dnetlib.uoamonitorservice.dao.StakeholderDAO;
import eu.dnetlib.uoamonitorservice.dto.ManageStakeholders;
import eu.dnetlib.uoamonitorservice.dto.StakeholderFull;
import eu.dnetlib.uoamonitorservice.dto.TopicFull;
import eu.dnetlib.uoamonitorservice.dto.UpdateUmbrella;
import eu.dnetlib.uoamonitorservice.entities.Stakeholder;
import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.handlers.BadRequestException;
import eu.dnetlib.uoamonitorservice.handlers.EntityNotFoundException;
import eu.dnetlib.uoamonitorservice.handlers.PathNotValidException;
import eu.dnetlib.uoamonitorservice.primitives.Action;
import eu.dnetlib.uoamonitorservice.primitives.StakeholderType;
import eu.dnetlib.uoamonitorservice.primitives.Umbrella;
import eu.dnetlib.uoamonitorservice.primitives.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class StakeholderService {

    private final StakeholderDAO dao;
    private final CommonService commonService;
    private final TopicService topicService;
    private final MongoTemplate mongoTemplate;

    @Autowired
    public StakeholderService(StakeholderDAO dao, CommonService commonService, TopicService topicService, MongoTemplate mongoTemplate) {
        this.dao = dao;
        this.commonService = commonService;
        this.topicService = topicService;
        this.mongoTemplate = mongoTemplate;
    }

    public Stakeholder findByAlias(String alias) {
        return this.dao.findByAlias(alias).orElseThrow(() -> new EntityNotFoundException("Stakeholder with alias: " + alias + " not found"));
    }

    public List<Stakeholder> findByDefaultId(String id) {
        return this.dao.findByDefaultIdAndCopyIsTrue(id);
    }

    public Stakeholder findByPath(String stakeholderId) {
        if (stakeholderId.equals("-1")) {
            return null;
        }
        return dao.findById(stakeholderId).orElseThrow(() -> new EntityNotFoundException("Stakeholder with id: " + stakeholderId + " not found"));
    }

    public List<String> getAllAliases(String type) {
        return this.getAll(type).stream().map(Stakeholder::getAlias).collect(Collectors.toList());
    }

    public List<Stakeholder> getAll(String type) {
        return this.dao.allStakeholders(StakeholderType.convert(type));
    }


    public List<Stakeholder> getAccessedStakeholders(List<Stakeholder> stakeholders, boolean isDefault) {
        return stakeholders.stream()
                .filter(stakeholder -> this.commonService.hasAccessAuthority(stakeholder.getType(), stakeholder.getAlias(), isDefault))
                .collect(Collectors.toList());
    }

    public ManageStakeholders getManageStakeholders(String type) {
        ManageStakeholders manageStakeholders = new ManageStakeholders();
        StakeholderType stakeholderType = StakeholderType.convert(type);
        manageStakeholders.setTemplates(this.getAccessedStakeholders(this.dao.defaultStakeholders(stakeholderType), true));
        manageStakeholders.setStandalone(this.getAccessedStakeholders(this.dao.standaloneStakeholders(stakeholderType), false));
        manageStakeholders.setDependent(this.getAccessedStakeholders(this.dao.dependentStakeholders(stakeholderType), false));
        manageStakeholders.setUmbrella(this.getAccessedStakeholders(this.dao.umbrellaStakeholders(stakeholderType), false));
        return manageStakeholders;
    }

    public List<Stakeholder> getVisibleStakeholders(String type, String defaultId) {
        return this.dao.browseStakeholders(StakeholderType.convert(type), defaultId).stream().filter(stakeholder ->
                        stakeholder.getVisibility() == Visibility.PUBLIC ||
                                stakeholder.getVisibility() == Visibility.RESTRICTED ||
                                this.commonService.hasAccessAuthority(stakeholder.getType(), stakeholder.getAlias(), false))
                .collect(Collectors.toList());
    }

    public StakeholderFull getFullStakeholder(Stakeholder stakeholder) {
        List<TopicFull> topics = stakeholder.getTopics().stream()
                .map(topicId -> topicService.getFullTopic(stakeholder.getType(), stakeholder.getAlias(), topicId))
                .collect(Collectors.toList());
        if (!stakeholder.isCopy() && stakeholder.getDefaultId() != null) {
            Stakeholder defaultStakeholder = this.findByPath(stakeholder.getDefaultId());
            if (defaultStakeholder != null) {
                topics = defaultStakeholder.getTopics().stream()
                        .map(topicId -> topicService.getFullTopic(stakeholder.getType(), stakeholder.getAlias(), topicId))
                        .collect(Collectors.toList());
            }
        }
        Umbrella<Stakeholder> umbrella = Umbrella.convert(stakeholder.getUmbrella(), this);
        return new StakeholderFull(stakeholder, topics, umbrella);
    }

    private List<Stakeholder> getOtherParents(String parent, String type, String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("alias").ne(parent).and("umbrella.children." + type).regex(id, "i"));
        return mongoTemplate.find(query, Stakeholder.class);
    }

    private Optional<Stakeholder> getActiveParent(String parent, String type, String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("alias").is(parent).and("umbrella.children." + type).regex(id, "i"));
        return Optional.ofNullable(mongoTemplate.findOne(query, Stakeholder.class));
    }

    public StakeholderFull getFullStakeholderWithParents(Stakeholder stakeholder, String type, String parent) {
        StakeholderFull stakeholderFull = this.getFullStakeholder(stakeholder);
        if (stakeholderFull != null) {
            stakeholderFull.setParent(this.getActiveParent(parent, type, stakeholder.getId())
                    .orElseThrow(() -> new EntityNotFoundException("Stakeholder with alias: " + stakeholder.getAlias() + " not found in stakeholder " + parent)));
            stakeholderFull.setOtherParents(this.getOtherParents(parent, type, stakeholder.getId()));
        }
        return stakeholderFull;
    }

    public Stakeholder buildStakeholder(Stakeholder stakeholder, String copyId) {
        if (stakeholder.getDefaultId() == null) {
            stakeholder.setCopy(false);
            if (copyId == null) {
                stakeholder.setTopics(new ArrayList<>());
            } else {
                Stakeholder copyFrom = this.findByPath(copyId);
                stakeholder.setTopics(copyFrom.getTopics().stream().map(this.topicService::copy).collect(Collectors.toList()));
            }
        } else {
            stakeholder.setTopics(new ArrayList<>());
            if (stakeholder.isCopy()) {
                Stakeholder defaultStakeholder = this.findByPath(stakeholder.getDefaultId());
                if (defaultStakeholder != null) {
                    stakeholder.setTopics(defaultStakeholder.getTopics().stream().map(this.topicService::build).collect(Collectors.toList()));
                }
            }
        }
        return this.save(stakeholder);
    }

    public Stakeholder save(Stakeholder stakeholder) {
        if (stakeholder.getId() != null) {
            Stakeholder old = this.findByPath(stakeholder.getId());
            stakeholder.setUmbrella(old.getUmbrella());
            stakeholder.setStandalone(old.isStandalone());
            stakeholder.setDefaultId(old.getDefaultId());
            if (!stakeholder.isCopy() && stakeholder.getDefaultId() != null) {
                stakeholder.getTopics().forEach(topic -> {
                    this.topicService.delete(stakeholder.getType(), topic, false);
                });
                stakeholder.setTopics(new ArrayList<>());
            } else {
                if (old.getTopics().isEmpty() && old.getDefaultId() != null) {
                    Stakeholder defaultStakeholder = this.findByPath(stakeholder.getDefaultId());
                    if (defaultStakeholder != null) {
                        stakeholder.setTopics(defaultStakeholder.getTopics().stream().map(this.topicService::build).collect(Collectors.toList()));
                    }
                }
                stakeholder.getTopics().forEach(this.topicService::find);
            }
        } else {
            stakeholder.setCreationDate(new Date());
        }
        stakeholder.setUpdateDate(new Date());
        return this.dao.save(stakeholder);
    }

    public StakeholderFull reorderTopics(Stakeholder stakeholder, List<String> topics) {
        if (this.commonService.hasEditAuthority(stakeholder.getType(), stakeholder.getAlias())) {
            topics.forEach(this.topicService::find);
            if (stakeholder.getTopics().size() == topics.size() && new HashSet<>(stakeholder.getTopics()).containsAll(topics)) {
                stakeholder.setTopics(topics);
                stakeholder.setUpdateDate(new Date());
                this.reorderChildren(stakeholder, topics);
                return this.getFullStakeholder(this.dao.save(stakeholder));
            } else {
                throw new EntityNotFoundException("Some topics dont exist in the stakeholder with id " + stakeholder.getId());
            }
        } else {
            this.commonService.unauthorized("You are not authorized to reorder topics in stakeholder with id: " + stakeholder.getId());
        }
        return null;
    }

    public void reorderChildren(Stakeholder defaultStakeholder, List<String> defaultTopics) {
        this.dao.findByDefaultIdAndCopyIsTrue(defaultStakeholder.getId()).stream().map(this::getFullStakeholder).forEach(stakeholder -> {
            this.reorderTopics(new Stakeholder(stakeholder, this),
                    this.commonService.reorder(defaultTopics, stakeholder.getTopics().stream().map(topic -> (Common) topic).collect(Collectors.toList())));
        });
    }

    public String delete(String id) {
        Stakeholder stakeholder = this.findByPath(id);
        if (this.commonService.hasDeleteAuthority(stakeholder.getType())) {
            this.dao.umbrellaStakeholders().forEach(umbrellaStakeholder -> {
                StakeholderType type = StakeholderType.valueOf(stakeholder.getType());
                List<String> ids = umbrellaStakeholder.getUmbrella().getChildren().get(type);
                if(ids != null && ids.contains(stakeholder.getId())) {
                    this.removeChild(umbrellaStakeholder.getId(), StakeholderType.valueOf(stakeholder.getType()), stakeholder.getId());
                }
            });
            this.dao.findByDefaultIdAndCopyIsTrue(stakeholder.getId()).forEach(child -> {
                this.delete(child.getId());
            });
            stakeholder.getTopics().forEach(topicId -> {
                this.topicService.delete(stakeholder.getType(), topicId, false);
            });
            this.dao.delete(id);
            return stakeholder.getAlias();
        } else {
            this.commonService.unauthorized("Delete stakeholder: You are not authorized to delete stakeholder with id: " + id);
        }
        return null;
    }

    public StakeholderFull changeVisibility(StakeholderFull stakeholder, Visibility visibility, Boolean propagate) {
        if (this.commonService.hasEditAuthority(stakeholder.getType(), stakeholder.getAlias())) {
            if (propagate) {
                stakeholder.setTopics(stakeholder.getTopics().stream().
                        map(topic -> this.topicService.changeVisibility(stakeholder.getType(), stakeholder.getAlias(), topic, visibility, true))
                        .collect(Collectors.toList()));
            }
            stakeholder.setVisibility(visibility);
            stakeholder.update(this.save(new Stakeholder(stakeholder, this)));
            return stakeholder;
        } else {
            this.commonService.unauthorized("Change stakeholder visibility: You are not authorized to update stakeholder with id: " + stakeholder.getId());
        }
        return null;
    }

    public StakeholderFull changeVisibility(Stakeholder stakeholder, Visibility visibility, Boolean propagate) {
        StakeholderFull stakeholderFull = this.getFullStakeholder(stakeholder);
        return this.changeVisibility(stakeholderFull, visibility, propagate);
    }

    public Umbrella<Stakeholder> updateUmbrella(Stakeholder stakeholder, UpdateUmbrella update) {
        if (this.commonService.hasEditAuthority(stakeholder.getType(), stakeholder.getAlias())) {
            if(update.getAction().equals(Action.ADD)) {
                if(update.getChild() != null) {
                    return this.addChild(stakeholder.getId(), update.getType(), update.getChild());
                } else {
                    return this.addType(stakeholder.getId(), update.getType());
                }
            } else if(update.getAction().equals(Action.REMOVE)) {
                if(update.getChild() != null) {
                    return this.removeChild(stakeholder.getId(), update.getType(), update.getChild());
                } else {
                    return this.removeType(stakeholder.getId(), update.getType());
                }
            } else if(update.getAction().equals(Action.UPDATE)) {
                if(update.getType() != null) {
                    return this.updateChildren(stakeholder.getId(), update.getType(), update.getChildren());
                } else {
                    return this.updateTypes(stakeholder.getId(), update.getTypes());
                }
            }
        } else {
            this.commonService.unauthorized("You are not authorized to update umbrella in stakeholder with id: " + stakeholder.getId());
        }
        return null;
    }

    public Umbrella<Stakeholder> addType(String id, StakeholderType type) {
        Stakeholder stakeholder = this.findByPath(id);
        Umbrella<String> umbrella = stakeholder.getUmbrellaOptional().orElseThrow(() -> new EntityNotFoundException("Umbrella not found in the stakeholder with id " + id));
        if (umbrella.addType(type)) {
            stakeholder.setUmbrella(umbrella);
            stakeholder.setUpdateDate(new Date());
            return this.getFullStakeholder(this.dao.save(stakeholder)).getUmbrella();
        }
        throw new BadRequestException("Cannot add type: " + type + " to stakeholder with id " + id);
    }

    public Umbrella<Stakeholder> removeType(String id, StakeholderType type) {
        Stakeholder stakeholder = this.findByPath(id);
        Umbrella<String> umbrella = stakeholder.getUmbrellaOptional().orElseThrow(() -> new EntityNotFoundException("Umbrella not found in the stakeholder with id " + id));
        if (umbrella.removeType(type)) {
            stakeholder.setUmbrella(umbrella);
            stakeholder.setUpdateDate(new Date());
            return this.getFullStakeholder(this.dao.save(stakeholder)).getUmbrella();
        }
        throw new BadRequestException("Cannot add type: " + type + " to stakeholder with id " + id);
    }

    public Umbrella<Stakeholder> addChild(String id, StakeholderType type, String childId) {
        Stakeholder stakeholder = this.findByPath(id);
        Umbrella<String> umbrella = stakeholder.getUmbrellaOptional().orElseThrow(() -> new EntityNotFoundException("Umbrella not found in the stakeholder with id " + id));
        Stakeholder child = this.findByPath(childId);
        if (child.getType().equals(type.name()) && umbrella.addChild(type, childId)) {
            stakeholder.setUmbrella(umbrella);
            stakeholder.setUpdateDate(new Date());
            return this.getFullStakeholder(this.dao.save(stakeholder)).getUmbrella();
        }
        throw new BadRequestException("Cannot add child: " + childId + " to stakeholder with id " + id);
    }

    public Umbrella<Stakeholder> removeChild(String id, StakeholderType type, String childId) {
        Stakeholder stakeholder = this.findByPath(id);
        Umbrella<String> umbrella = stakeholder.getUmbrellaOptional().orElseThrow(() -> new EntityNotFoundException("Umbrella not found in the stakeholder with id " + id));
        Stakeholder child = this.findByPath(childId);
        if (child.getType().equals(type.name()) && umbrella.removeChild(type, childId)) {
            stakeholder.setUmbrella(umbrella);
            stakeholder.setUpdateDate(new Date());
            return this.getFullStakeholder(this.dao.save(stakeholder)).getUmbrella();
        }
        throw new BadRequestException("Cannot remove child: " + childId + " to stakeholder with id " + id);
    }

    public Umbrella<Stakeholder> updateTypes(String id, List<StakeholderType> types) {
        Stakeholder stakeholder = this.findByPath(id);
        Umbrella<String> umbrella = stakeholder.getUmbrellaOptional().orElseThrow(() -> new EntityNotFoundException("Umbrella not found in the stakeholder with id " + id));
        if (stakeholder.getUmbrella().update(types)) {
            stakeholder.setUmbrella(umbrella);
            stakeholder.setUpdateDate(new Date());
            return this.getFullStakeholder(this.dao.save(stakeholder)).getUmbrella();
        }
        throw new BadRequestException("Cannot update types in umbrella of stakeholder with id " + id);
    }

    public Umbrella<Stakeholder> updateChildren(String id, StakeholderType type, List<String> children) {
        Stakeholder stakeholder = this.findByPath(id);
        Umbrella<String> umbrella = stakeholder.getUmbrellaOptional().orElseThrow(() -> new EntityNotFoundException("Umbrella not found in the stakeholder with id " + id));
        if (stakeholder.getUmbrella().update(type, children)) {
            stakeholder.setUmbrella(umbrella);
            stakeholder.setUpdateDate(new Date());
            return this.getFullStakeholder(this.dao.save(stakeholder)).getUmbrella();
        }
        throw new BadRequestException("Cannot update children of " + type + " in umbrella of stakeholder with id " + id);
    }
}
