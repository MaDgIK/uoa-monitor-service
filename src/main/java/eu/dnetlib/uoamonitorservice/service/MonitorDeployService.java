package eu.dnetlib.uoamonitorservice.service;

import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DBObject;
import eu.dnetlib.uoamonitorservice.configuration.GlobalVars;
import eu.dnetlib.uoamonitorservice.configuration.mongo.MongoConnection;
import eu.dnetlib.uoamonitorservice.configuration.properties.MongoConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class MonitorDeployService {
    private final MongoConnection mongoConnection;
    private final MongoConfig mongoConfig;
    private final GlobalVars globalVars;

    @Autowired
    public MonitorDeployService(MongoConnection mongoConnection, MongoConfig mongoConfig, GlobalVars globalVars) {
        this.mongoConnection = mongoConnection;
        this.mongoConfig = mongoConfig;
        this.globalVars = globalVars;
    }

    public Map<String, String> checkEverything() {
        Map<String, String> response = new HashMap<>();

        MongoTemplate mt = mongoConnection.getMongoTemplate();
        DBObject ping = new BasicDBObject("ping", "1");
        try {
            CommandResult answer = mt.getDb().command(ping);
            response.put("Mongo try: error", answer.getErrorMessage());
        } catch (Exception e) {
            response.put("Mongo catch: error", e.getMessage());
        }

        response.put("monitorservice.mongodb.database", mongoConfig.getDatabase());
        response.put("monitorservice.mongodb.host", mongoConfig.getHost());
        response.put("monitorservice.mongodb.port", mongoConfig.getPort() + "");
        response.put("monitorservice.mongodb.username", mongoConfig.getUsername() == null ? null : "[unexposed value]");
        response.put("monitorservice.mongodb.password", mongoConfig.getPassword() == null ? null : "[unexposed value]");

        if (globalVars.date != null) {
            response.put("Date of deploy", globalVars.date.toString());
        }
        if (globalVars.getBuildDate() != null) {
            response.put("Date of build", globalVars.getBuildDate());
        }
        if (globalVars.getVersion() != null) {
            response.put("Version", globalVars.getVersion());
        }

        return response;
    }
}
