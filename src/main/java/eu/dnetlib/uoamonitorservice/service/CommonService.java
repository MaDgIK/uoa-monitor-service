package eu.dnetlib.uoamonitorservice.service;

import eu.dnetlib.uoaadmintoolslibrary.handlers.ForbiddenException;
import eu.dnetlib.uoaadmintoolslibrary.handlers.UnauthorizedException;
import eu.dnetlib.uoaauthorizationlibrary.security.AuthorizationService;
import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.primitives.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CommonService {

    AuthorizationService authorizationService;

    @Autowired
    public CommonService(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    public boolean hasAccessAuthority(String type, String alias, boolean isDefault) {
        if(this.authorizationService.isPortalAdmin() || this.authorizationService.isCurator(type)) {
            return true;
        } else if(!isDefault) {
            return this.authorizationService.isManager(type, alias);
        }
        return false;
    }

    public boolean hasDeleteAuthority(String type) {
        return this.authorizationService.isPortalAdmin() || this.authorizationService.isCurator(type);
    }

    public boolean hasCreateAuthority(String type) {
        return this.authorizationService.isPortalAdmin() || this.authorizationService.isCurator(type);
    }

    public boolean hasEditAuthority(String type, String alias) {
        return this.authorizationService.isPortalAdmin() || this.authorizationService.isCurator(type) || this.authorizationService.isManager(type, alias);
    }

    public boolean hasVisibilityAuthority(String type, String alias, Common common) {
        if(authorizationService.isPortalAdmin() || authorizationService.isCurator(type) || authorizationService.isManager(type, alias)) {
            return true;
        } else if(authorizationService.isMember(type, alias) && common.getVisibility() != Visibility.PRIVATE) {
            return true;
        } else {
            return common.getVisibility() == Visibility.PUBLIC;
        }
    }

    public List<String> reorder(List<String> defaultIds, List<Common> commons) {
        Map<String, Integer> map = new HashMap<>();
        for(int i = 0; i < commons.size(); i++) {
            if (!defaultIds.contains(commons.get(i).getDefaultId())) {
                map.put(commons.get(i).getId(), i);
            }
        }
        commons = commons.stream().filter(common -> defaultIds.contains(common.getDefaultId())).collect(Collectors.toList());;
        commons.sort(Comparator.comparingInt(common -> defaultIds.indexOf(common.getDefaultId())));
        List<String> ids = commons.stream().map(Common::getId).collect(Collectors.toList());
        map.keySet().forEach(key -> {
            ids.add(map.get(key), key);
        });
        return ids;
    }

    public void unauthorized(String message) {
        if(authorizationService.getAaiId() != null) {
            throw new ForbiddenException(message);
        } else {
            throw new UnauthorizedException(message);
        }
    }
}
