package eu.dnetlib.uoamonitorservice.service;

import eu.dnetlib.uoamonitorservice.dao.*;
import eu.dnetlib.uoamonitorservice.dto.SectionFull;
import eu.dnetlib.uoamonitorservice.entities.Section;
import eu.dnetlib.uoamonitorservice.entities.Stakeholder;
import eu.dnetlib.uoamonitorservice.entities.SubCategory;
import eu.dnetlib.uoamonitorservice.generics.Common;
import eu.dnetlib.uoamonitorservice.handlers.EntityNotFoundException;
import eu.dnetlib.uoamonitorservice.handlers.PathNotValidException;
import eu.dnetlib.uoamonitorservice.primitives.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SectionService {

    private final StakeholderDAO stakeholderDAO;
    private final TopicDAO topicDAO;
    private final CategoryDAO categoryDAO;
    private final SubCategoryDAO subCategoryDAO;
    private final SectionDAO dao;

    private final IndicatorService indicatorService;
    private final CommonService commonService;

    @Autowired
    public SectionService(StakeholderDAO stakeholderDAO, TopicDAO topicDAO, CategoryDAO categoryDAO, SubCategoryDAO subCategoryDAO, SectionDAO dao, IndicatorService indicatorService, CommonService commonService) {
        this.stakeholderDAO = stakeholderDAO;
        this.topicDAO = topicDAO;
        this.categoryDAO = categoryDAO;
        this.subCategoryDAO = subCategoryDAO;
        this.dao = dao;
        this.indicatorService = indicatorService;
        this.commonService = commonService;
    }

    public Section find(String id) {
        return dao.findById(id).orElseThrow(() -> new EntityNotFoundException("Section with id: " + id + " not found"));
    }

    public Section findByPath(SubCategory subCategory, String sectionId) {
        if (!subCategory.getNumbers().contains(sectionId) && !subCategory.getCharts().contains(sectionId)) {
            throw new PathNotValidException("Section with id: " + sectionId + " not found in SubCategory: " + subCategory.getId());
        }
        return this.dao.findById(sectionId).orElseThrow(() -> new EntityNotFoundException("Section with id: " + sectionId + " not found"));
    }

    public SectionFull getFullSection(String type, String alias, Section section) {
        return new SectionFull(section, section.getIndicators().stream()
                .map(indicatorId -> this.indicatorService.getIndicator(type, alias, indicatorId))
                .collect(Collectors.toList()));
    }

    public SectionFull getFullSection(String type, String alias, String id) {
        Section section = this.find(id);
        return this.getFullSection(type, alias, section);
    }

    public SectionFull saveFull(SectionFull sectionFull) {
        sectionFull.setIndicators(sectionFull.getIndicators().stream().map(this.indicatorService::save).collect(Collectors.toList()));
        Section section = this.save(new Section(sectionFull));
        return new SectionFull(section, sectionFull.getIndicators());
    }

    public String build(String id) {
        Section section = this.find(id);
        Section copy = section.copy();
        copy.setIndicators(section.getIndicators().stream().map(this.indicatorService::build).collect(Collectors.toList()));
        return this.save(copy).getId();
    }

    public String copy(String id) {
        Section section = this.find(id);
        Section copy = new Section(section);
        copy.setId(null);
        copy.setIndicators(section.getIndicators().stream().map(this.indicatorService::copy).collect(Collectors.toList()));
        return this.save(copy).getId();
    }

    public Section save(Section section) {
        if (section.getId() != null) {
            section.setIndicators(this.find(section.getId()).getIndicators());
        } else {
            section.setCreationDate(new Date());
        }
        section.setUpdateDate(new Date());
        section.getIndicators().forEach(this.indicatorService::find);
        return this.dao.save(section);
    }

    public void saveBulk(Stakeholder stakeholder, SubCategory subCategory, List<SectionFull> sections) {
        if (this.commonService.hasCreateAuthority(stakeholder.getType())) {
            sections = sections.stream().map(this::saveFull).collect(Collectors.toList());
            sections.forEach(section -> {
                this.addSection(subCategory, section.getId());
                this.createChildren(subCategory, new Section(section), -1);
            });
        } else {
            this.commonService.unauthorized("You are not authorized to create sections in stakeholder with id: " + stakeholder.getId());
        }
    }

    public SectionFull save(Stakeholder stakeholder, SubCategory subCategory, Section section, int index) {
        section.setStakeholderAlias(stakeholder.getAlias());
        if (section.getId() != null) {
            if (this.commonService.hasEditAuthority(stakeholder.getType(), stakeholder.getAlias())) {
                section.setIndicators(this.find(section.getId()).getIndicators());
                this.updateChildren(section);
                section = this.save(section);
            } else {
                this.commonService.unauthorized("You are not authorized to update stakeholder with id: " + stakeholder.getId());
            }
        } else {
            if (this.commonService.hasCreateAuthority(stakeholder.getType())) {
                section = this.save(section);
                this.createChildren(subCategory, section, index);
                this.addSection(subCategory, section.getId(), index);
            } else {
                this.commonService.unauthorized("You are not authorized to create a section in stakeholder with id: " + stakeholder.getId());
            }
        }
        return this.getFullSection(stakeholder.getType(), stakeholder.getAlias(), section);
    }

    public void createChildren(SubCategory defaultSubCategory, Section section, int index) {
        this.subCategoryDAO.findByDefaultId(defaultSubCategory.getId()).forEach(subCategory -> {
            this.categoryDAO.findBySubCategoriesContaining(subCategory.getId()).forEach(category -> {
                this.topicDAO.findByCategoriesContaining(category.getId()).forEach(topic -> {
                    this.stakeholderDAO.findByTopicsContaining(topic.getId()).forEach(stakeholder -> {
                        this.save(stakeholder, subCategory, section.copy(), index);
                    });
                });
            });
        });
        section.getIndicators().forEach(indicator -> {
            this.indicatorService.createChildren(section, this.indicatorService.find(indicator));
        });
    }

    public void updateChildren(Section section) {
        this.dao.findByDefaultId(section.getId()).forEach(child -> {
            this.save(section.override(child, this.find(section.getId())));
        });
    }

    public SectionFull reorderIndicators(Stakeholder stakeholder, Section section, List<String> indicators) {
        return this.reorderIndicators(stakeholder, section, indicators, true);
    }

    public SectionFull reorderIndicators(Stakeholder stakeholder, Section section, List<String> indicators, boolean reorderChildren) {
        if (this.commonService.hasEditAuthority(stakeholder.getType(), stakeholder.getAlias())) {
            indicators.forEach(this.indicatorService::find);
            if (section.getIndicators().size() == indicators.size() && new HashSet<>(section.getIndicators()).containsAll(indicators)) {
                section.setIndicators(indicators);
                section.setUpdateDate(new Date());
                if(reorderChildren) {
                    this.reorderChildren(stakeholder, section, indicators);
                }
                return this.getFullSection(stakeholder.getType(), stakeholder.getAlias(), this.dao.save(section));
            } else {
                throw new EntityNotFoundException("Some indicators dont exist in the section with id " + section.getId());
            }
        } else {
            this.commonService.unauthorized("You are not authorized to reorder indicators in section with id: " + section.getId());
        }
        return null;
    }

    public void reorderChildren(Stakeholder defaultStakeholder, Section defaultSection, List<String> defaultIndicators) {
        this.stakeholderDAO.findByDefaultIdAndCopyIsTrue(defaultStakeholder.getId()).forEach(stakeholder -> {
            this.dao.findByDefaultId(defaultSection.getId()).stream().map(section -> this.getFullSection(stakeholder.getType(), stakeholder.getAlias(), section)).forEach(section -> {
                this.reorderIndicators(stakeholder, new Section(section),
                        this.commonService.reorder(defaultIndicators, section.getIndicators().stream().map(indicator -> (Common) indicator).collect(Collectors.toList())));
            });
        });
    }

    public void delete(String type, Section section, boolean remove) {
        if (this.commonService.hasDeleteAuthority(type)) {
            this.dao.findByDefaultId(section.getId()).forEach(child -> {
                this.delete(type, child.getId(), remove);
            });
            section.getIndicators().forEach(indicatorId -> {
                this.indicatorService.delete(type, indicatorId, false);
            });
            if (remove) {
                this.removeSection(section.getId());
            }
            this.dao.delete(section);
        } else {
            this.commonService.unauthorized("Delete section: You are not authorized to delete section with id: " + section.getId());
        }
    }

    public void delete(String type, String id, boolean remove) {
        Section section = this.find(id);
        this.delete(type, section, remove);
    }

    public void addSection(SubCategory subCategory, String id) {
        this.addSection(subCategory, id, -1);
    }

    public void addSection(SubCategory subCategory, String id, int index) {
        if (this.find(id).isNumber()) {
            subCategory.addNumber(id, index);
        } else {
            subCategory.addChart(id, index);
        }
        subCategory.setUpdateDate(new Date());
        this.subCategoryDAO.save(subCategory);
    }

    public void removeSection(String id) {
        this.subCategoryDAO.findByNumbersContaining(id).forEach(subCategory -> {
            subCategory.removeNumber(id);
            subCategory.setUpdateDate(new Date());
            this.subCategoryDAO.save(subCategory);
        });
        this.subCategoryDAO.findByChartsContaining(id).forEach(subCategory -> {
            subCategory.removeChart(id);
            subCategory.setUpdateDate(new Date());
            this.subCategoryDAO.save(subCategory);
        });
    }

    public SectionFull changeVisibility(String type, String alias, SectionFull section, Visibility visibility) {
        if (this.commonService.hasEditAuthority(type, alias)) {
            section.setIndicators(section.getIndicators().stream()
                    .map(indicator -> this.indicatorService.changeVisibility(type, alias, indicator, visibility))
                    .collect(Collectors.toList()));
            section.update(this.save(new Section(section)));
            return section;
        } else {
            this.commonService.unauthorized("Change section visibility: You are not authorized to update section with id: " + section.getId());
        }
        return null;
    }
}
