package eu.dnetlib.uoamonitorservice.application;

import eu.dnetlib.uoaauthorizationlibrary.configuration.AuthorizationConfiguration;
import eu.dnetlib.uoanotificationservice.configuration.NotificationConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;


@SpringBootApplication(scanBasePackages = {"eu.dnetlib.uoamonitorservice"})
@PropertySources({
        @PropertySource("classpath:authorization.properties"),
        @PropertySource("classpath:admintoolslibrary.properties"),
        @PropertySource("classpath:notification.properties"),
        @PropertySource("classpath:monitorservice.properties"),
        @PropertySource(value = "classpath:dnet-override.properties", ignoreResourceNotFound = true)
})
@Import({
        NotificationConfiguration.class,
        AuthorizationConfiguration.class
})
public class UoaMonitorServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(UoaMonitorServiceApplication.class, args);
    }
}
